//This file was generated from (Academic) UPPAAL 4.1.18 (rev. 5444), November 2013

/*

*/
E<> ClientThread(0).WaitOnTransfer && ClientThread(1).WaitOnTransfer

/*

*/
E<> ClientAgent.bids == N

/*

*/
E<> ClientAgent.noneAccepted == true && ClientAgent.answers == N

/*

*/
E<> ClientAgent.TransferSuccess

/*

*/
A[] not deadlock
