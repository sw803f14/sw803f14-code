package dk.wukong.mymodule.app;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.HashMap;
import java.util.Map;

import dk.wukong.sunwukong.agent_lib.agent_interface.Agent;
import dk.wukong.sunwukong.agent_lib.networking.ClientMessenger;
import dk.wukong.sunwukong.agent_lib.networking.ClientRunnable;
import dk.wukong.sunwukong.agent_lib.networking.ServerMessenger;
import dk.wukong.sunwukong.agent_lib.networking.ServerRunnable;
import dk.wukong.sunwukong.agent_lib.wifi_direct.WifiDirectAgent;


public class MainActivity extends Activity {
    private final static String TAG = "Main: ";
    private WifiDirectAgent agent;
    private Button msgButton;
    private Button connectionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.agent = new WifiDirectAgent(this, new Agent.OnCFPListener() {
            @Override
            public void onCFP(Map<String, String> data, ServerMessenger messenger) {
                //Do something with the CFP data and respond
                //For now just log the data and make a bid with the exact same map
                Log.d(TAG, String.valueOf(data));
                int someCost = 10;
                messenger.bid(someCost, new Agent.ApprovalListener() {

                    @Override
                    public void onAccept() {
                        Log.d(TAG, "Bid accepted");
                    }

                    @Override
                    public void onReject() {
                        Log.d(TAG, "Bid rejected");
                    }
                });
            }
        });

        msgButton = (Button)findViewById(R.id.msgButton);
        msgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> data = new HashMap<>();
                data.put("first_key", "first_value");
                data.put("second_key", "second_value");
                agent.callForProposals(data, new Agent.BidListener() {
                    @Override
                    public void onBid(int cost, ClientMessenger messenger) {
                        //Collect the bid and compare to other agents (IF WE HAD ANY)
                        //For now just accept the bid
                        messenger.accept(new Agent.TransferListener() {
                            @Override
                            public void onTransferSuccess(String path) {
                                //Transfer completed, use the path to access the data
                                Log.d(TAG, "Transfer success, path: " + path);
                            }

                            @Override
                            public void onTransferFailure() {
                                //Transfer failed, retry?
                                //Maybe add integer constants to indicate reason...
                                Log.d(TAG, "Transfer failed");
                            }
                        });
                    }

                    @Override
                    public void onReject() {
                        //No bid received from this agent
                        Log.d(TAG, "Proposal rejected on WDirectAgent network");
                    }
                });
            }
        });

        connectionButton = (Button)findViewById(R.id.connecteButton);
        connectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agent.doConnect();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        agent.onDestroy();
        super.onDestroy();
    }

}