package dk.wukong.sunwukong.agent_lib.networking;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;

import dk.wukong.sunwukong.agent_lib.agent_interface.Agent;


/**
 * Created by nattfari and vincent on 5/15/14.
 */
public class ServerRunnable implements Runnable {

    private static final String TAG = "ServerRunnable: ";
    private Socket socket;
    private NetworkManager networkManager;
    private Agent.ApprovalListener approvalListener;
    private Agent.OnCFPListener cfpListener;
    private DataInputStream inStream;
    private PrintWriter outStream;
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;
    public Handler handler;

    public ServerRunnable(Socket tcpSock, NetworkManager server, Agent.OnCFPListener listener) {
        this.socket = tcpSock;
        this.networkManager = server;
        this.cfpListener = listener;
    }

    public void setApprovalListener(Agent.ApprovalListener listener) {
        this.approvalListener = listener;
    }

    @Override
    public void run() {
        Log.d(TAG, "ServerHandler is now running");
        try {
            Looper.prepare();
            handler = new Handler() {
                public void handleMessage(Message msg) {
                    int what = msg.what;
                    switch (what) {
                        case(Agent.CODE_BID):
                            //Bid
                            int cost = msg.arg1;
                            bid(cost, approvalListener);
                            break;
                        case(Agent.CODE_REJECT):
                            //Reject
                            reject();
                            break;
                    }
                }
            };
            Looper.loop();
            inStream = new DataInputStream(socket.getInputStream());
            outStream = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectInputStream = new ObjectInputStream(socket.getInputStream());

            while (!Thread.currentThread().isInterrupted()) {
                String input = readLine(inStream);
                String output = protocol(input);
                Log.d(TAG, "ServerOutput: " + output);
            }

        } catch (IOException ex) {
            Log.e(TAG, "Streams could not be opened");
            ex.printStackTrace();
        } finally {
            Looper.myLooper().quit();
            try {
                if(inStream != null) {
                    inStream.close();
                }
                if(outStream != null) {
                    outStream.close();
                }
                if(socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                Log.e(TAG, "Problem closing streams and socket");
                ex.printStackTrace();
            }

        }
    }

    private void bid(int cost, Agent.ApprovalListener approvalListener) {
        Log.d(TAG, "Making a bid");
        this.approvalListener = approvalListener;
        outStream.println(Agent.MSG_BID);
        outStream.println(String.valueOf(cost));
    }

    private void reject() {
        Log.d(TAG, "Rejecting");
        outStream.println(Agent.MSG_REJECT);
        Thread.currentThread().interrupt();
    }

    private String readLine(DataInputStream in) {
        StringBuffer buffer = new StringBuffer();
        String ret = null;
        try {
            char next = in.readChar();
            while(next != '\n' && next != '\r') {
                buffer.append(next);
                next = in.readChar();
            }
            ret = buffer.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return ret;
    }

    private String protocol(String input) {
        String output = input;

        switch (input) {
            case(Agent.MSG_CFP):
                Log.d(TAG, "CFP gotten");
                //CFP gotten
                try {
                    HashMap<String, String> inputData = (HashMap<String, String>) objectInputStream.readObject();
                    cfpListener.onCFP(inputData, new ServerMessenger(handler, this));
                } catch (IOException | ClassNotFoundException ex) {
                    Log.d(TAG, "CFP not followed by metadata");
                    ex.printStackTrace();
                }
                break;
            case(Agent.MSG_ACCEPT):
                Log.d(TAG, "Accept gotten");
                //Accept gotten
                approvalListener.onAccept();
                //TODO: Send to service and wait for command to transfer data
                break;
            case(Agent.MSG_REJECT):
                Log.d(TAG, "Reject gotten");
                //Reject gotten
                approvalListener.onReject();
                Thread.currentThread().interrupt();
                break;
            case (Agent.MSG_REQ):
                Log.d(TAG, "REQUEST gotten");
                //Request for the networklist gotten
                try {
                    objectOutputStream.reset();
                    objectOutputStream.writeObject(networkManager.getAddressMap());
                    objectOutputStream.flush();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            case (Agent.MSG_REG):
                //Register service in map
                InetSocketAddress remoteAddress = (InetSocketAddress) socket.getRemoteSocketAddress();
                InetAddress inetAddress = remoteAddress.getAddress();
                int inPort = remoteAddress.getPort();
                networkManager.registerPeerService(inetAddress, inPort);
                break;
            default:
                //Map?
                break;
        }
        return output;
    }
}
