package dk.wukong.sunwukong.agent_lib.wifi_direct;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;

import dk.wukong.sunwukong.agent_lib.agent_interface.WifiEventListener;


public class WDirectReceiver extends BroadcastReceiver{
    private static final String TAG = "WDirectReceiver: ";

    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;
    private WifiEventListener wifiEventListener;

    public WDirectReceiver(WifiP2pManager man, WifiP2pManager.Channel chan, WifiEventListener eventListener) {
        super();
        this.manager = man;
        this.channel = chan;
        this.wifiEventListener = eventListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        switch(intent.getAction()){
            case WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION:
                wifiEventListener.onStateChanged(intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1));
                break;
            case WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION:
                if(manager != null) {
                    wifiEventListener.onConnectionChanged(((NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO)).isConnected());
                }
                break;
        }
    }
}
