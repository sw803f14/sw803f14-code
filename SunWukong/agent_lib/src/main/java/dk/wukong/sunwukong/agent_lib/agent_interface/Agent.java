package dk.wukong.sunwukong.agent_lib.agent_interface;

import java.util.HashMap;
import java.util.Map;

import dk.wukong.sunwukong.agent_lib.networking.ClientMessenger;
import dk.wukong.sunwukong.agent_lib.networking.ClientRunnable;
import dk.wukong.sunwukong.agent_lib.networking.ServerMessenger;
import dk.wukong.sunwukong.agent_lib.networking.ServerRunnable;

/**
 * Created by nattfari on 4/25/14.
 */
public interface Agent {
    //Server and client messages
    public static final String MSG_BID = "BID";
    public static final String MSG_REG = "REG_ME";
    public static final String MSG_REQ = "REQ_LST";
    public static final String MSG_CFP = "CFP";
    public static final String MSG_ACCEPT = "ACCEPT";
    public static final String MSG_REJECT = "REJECT";

    public static final int CODE_BID = 1;
    public static final int CODE_ACCEPT = 2;
    public static final int CODE_REJECT = 3;
    public static final int CODE_CFP = 4;
    public static final int CODE_REG = 5;
    public static final int CODE_REQ = 6;

    void callForProposals(HashMap<String, String> data, BidListener bidListener);

    public interface Client {
        void accept(TransferListener transferListener);
        void reject();
    }

    public interface Server {
        void bid(int cost, ApprovalListener approvalListener);
        void reject();
    }

    public interface OnCFPListener {
        void onCFP(Map<String, String> data, ServerMessenger messenger);
    }

    public interface BidListener {
        void onBid(int cost, ClientMessenger messenger);
        void onReject();
    }

    public interface ApprovalListener {
        void onAccept();
        void onReject();
    }

    public interface TransferListener {
        void onTransferSuccess(String path);
        void onTransferFailure();
    }

}
