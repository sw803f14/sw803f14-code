package dk.wukong.sunwukong.agent_lib.networking;

import android.os.Handler;
import android.os.Message;

import java.util.Map;

import dk.wukong.sunwukong.agent_lib.agent_interface.Agent;
import dk.wukong.sunwukong.agent_lib.agent_interface.ClientBidListener;

/**
 * Created by nattfari on 5/23/14.
 */
public class ClientMessenger implements Agent.Client {
    private Handler handler;
    private ClientRunnable runnable;

    public ClientMessenger(Handler h, ClientRunnable r) {
        this.handler = h;
        this.runnable = r;
    }

    @Override
    public void accept(Agent.TransferListener transferListener) {
        runnable.setTransferListener(transferListener);
        Message acceptMsg = Message.obtain(handler, Agent.CODE_ACCEPT);
        handler.sendMessage(acceptMsg);
    }

    @Override
    public void reject() {
        Message rejectMsg = Message.obtain(handler, Agent.CODE_REJECT);
        handler.sendMessage(rejectMsg);

    }
}
