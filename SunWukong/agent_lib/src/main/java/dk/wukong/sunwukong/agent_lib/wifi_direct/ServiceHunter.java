package dk.wukong.sunwukong.agent_lib.wifi_direct;

import android.annotation.TargetApi;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


/**
 * Created by nattfari on 4/21/14.
 */
public class ServiceHunter {
    private static final String TAG = "WifiDirect_Agent: ";

    public static final String SERVICE_INSTANCE = "_sunWukong";
    public static final String SERVICE_TYPE = "_data_transfer.tcp";
    public static final String KEY_NAME = "service_name";
    public static final String KEY_PORT = "listen_port";
    public static final String KEY_NODEID = "node_id";
    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;
    private WifiP2pDnsSdServiceRequest serviceRequest;

    public ServiceHunter(WifiP2pManager man, WifiP2pManager.Channel chan) {
        this.manager = man;
        this.channel = chan;
    }

    @TargetApi(16)
    public void registerService(int listenPort, String deviceName) {
        Log.d(TAG, "Register port: " + String.valueOf(listenPort) + " name: " + deviceName);

        Map<String, String> serviceRecord = new HashMap<String, String>();
        serviceRecord.put(KEY_PORT, String.valueOf(listenPort));
        serviceRecord.put(KEY_NODEID, deviceName + "@node" + (int)(Math.random() * 1000));

        WifiP2pDnsSdServiceInfo serviceInfo = WifiP2pDnsSdServiceInfo.newInstance(SERVICE_INSTANCE, SERVICE_TYPE, serviceRecord);

        manager.addLocalService(channel, serviceInfo, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "P2PService was successfully registered");
            }

            @Override
            public void onFailure(int reason) {
                switch(reason){
                    case(WifiP2pManager.BUSY):
                        Log.e(TAG, "P2PService could not be registered. Framework is busy and unable to service the request.");
                        break;
                    case(WifiP2pManager.ERROR):
                        Log.e(TAG, "P2PService could not be registered. Operation failed due to an internal error");
                        break;
                    case(WifiP2pManager.P2P_UNSUPPORTED):
                        Log.e(TAG, "P2PService could not be registered. P2P is not supported on this device");
                        break;
                }
            }
        });
    }

    @TargetApi(16)
    public void unregisterService() {
        manager.clearLocalServices(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Unregistering local services was succesful");
            }

            @Override
            public void onFailure(int reason) {
                switch(reason){
                    case(WifiP2pManager.BUSY):
                        Log.e(TAG, "Unregistering local service failed. Framework is busy and unable to service the request.");
                        break;
                    case(WifiP2pManager.ERROR):
                        Log.e(TAG, "Unregistering local service failed. Operation failed due to an internal error");
                        break;
                    case(WifiP2pManager.P2P_UNSUPPORTED):
                        Log.e(TAG, "Unregistering local service failed. P2P is not supported on this device");
                        break;
                }
            }
        });
    }

    @TargetApi(16)
    public void startDiscovery(final LinkedBlockingQueue<WifiP2pDevice> connectionQueue, final HashMap<String, Map<String, String>> serviceLookupTable) {
        WifiP2pManager.DnsSdTxtRecordListener recordListener = new WifiP2pManager.DnsSdTxtRecordListener() {
            @Override
            public void onDnsSdTxtRecordAvailable(String fullDomainName, Map<String, String> txtRecordMap, WifiP2pDevice srcDevice) {
                    Log.d(TAG, "Service named: " + fullDomainName + " with values: " + String.valueOf(txtRecordMap.values()));
                    serviceLookupTable.put(srcDevice.deviceAddress, txtRecordMap);
                    if(connectionQueue.offer(srcDevice)) {
                        //Do nothing, the operation succeeded
                    } else {
                        try {
                            connectionQueue.offer(srcDevice, 200, TimeUnit.MILLISECONDS);
                        } catch(InterruptedException iEx) {
                            Log.d(TAG, "Offering element to connectionQueue was interrupted");
                            //Swallow it
                            //This is fallback, if it fails... fuck it
                        }
                    }
            }
        };

        WifiP2pManager.DnsSdServiceResponseListener serviceResponseListener = new WifiP2pManager.DnsSdServiceResponseListener() {
            @Override
            public void onDnsSdServiceAvailable(String instanceName, String registrationType, WifiP2pDevice srcDevice) {
                Log.d(TAG, "Service available: " + instanceName + " : " + srcDevice.deviceAddress);

            }
        };

        manager.setDnsSdResponseListeners(channel, serviceResponseListener, recordListener);

        //We only look for our service, hence the parameters in 'newInstance(SERVICE_INSTANCE, SERVICE_TYPE);'
        serviceRequest = WifiP2pDnsSdServiceRequest.newInstance(SERVICE_INSTANCE, SERVICE_TYPE);
        manager.addServiceRequest(channel, serviceRequest, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Service request added successfully");
            }

            @Override
            public void onFailure(int reason) {
                switch (reason) {
                    case WifiP2pManager.P2P_UNSUPPORTED:
                        Log.d(TAG, "Service request addition failed, P2P is not supported");
                        break;
                    case WifiP2pManager.BUSY:
                        Log.d(TAG, "Service request addition failed, P2PManager is busy");
                        break;
                    case WifiP2pManager.ERROR:
                        Log.d(TAG, "Service request addition failed, Error occurred");
                        break;
                }
            }
        });

        manager.discoverServices(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Service discovery started");
            }

            @Override
            public void onFailure(int reason) {
                switch (reason) {
                    case WifiP2pManager.P2P_UNSUPPORTED:
                        Log.d(TAG, "Service discovery failed to start, P2P is not supported");
                        break;
                    case WifiP2pManager.BUSY:
                        Log.d(TAG, "Service discovery failed to start, P2PManager is busy");
                        break;
                    case WifiP2pManager.ERROR:
                        Log.d(TAG, "Service discovery failed to start, Error occurred");
                        break;
                    }
            }
        });
    }

    @TargetApi(16)
    public void stopDiscovery() {
        manager.clearServiceRequests(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Service discovery was successfully stopped");
            }

            @Override
            public void onFailure(int reason) {
                switch(reason){
                    case(WifiP2pManager.BUSY):
                        Log.e(TAG, "Service discovery could not be stopped. Framework is busy and unable to service the request.");
                        break;
                    case(WifiP2pManager.ERROR):
                        Log.e(TAG, "Service discovery could not be stopped. Operation failed due to an internal error");
                        break;
                    case(WifiP2pManager.P2P_UNSUPPORTED):
                        Log.e(TAG, "Service discovery could not be stopped. P2P is not supported on this device");
                        break;
                }
            }
        });
    }

    @TargetApi(16)
    public void resetDiscovery(final LinkedBlockingQueue<WifiP2pDevice> connectionQueue, final HashMap<String, Map<String, String>> serviceLookupTable) {
        manager.clearServiceRequests(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                startDiscovery(connectionQueue, serviceLookupTable);
            }

            @Override
            public void onFailure(int reason) {
                switch (reason) {
                    case(WifiP2pManager.BUSY):
                        startDiscovery(connectionQueue, serviceLookupTable);
                        break;
                }
            }
        });
    }
}
