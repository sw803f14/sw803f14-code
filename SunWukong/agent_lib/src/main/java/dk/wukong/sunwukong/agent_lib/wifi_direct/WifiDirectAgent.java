package dk.wukong.sunwukong.agent_lib.wifi_direct;

import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Pair;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import dk.wukong.sunwukong.agent_lib.agent_interface.Agent;
import dk.wukong.sunwukong.agent_lib.agent_interface.WifiEventListener;
import dk.wukong.sunwukong.agent_lib.networking.NetworkManager;

/**
 * Created by nattfari on 4/17/14.
 */
public class WifiDirectAgent implements Agent {
    private static final String TAG = "WifiDirect: ";

    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;
    private WDirectReceiver wDirectReceiver = null;
    private Context context;
    private NetworkManager networkManager;
    private ExecutorService serverExec;
    private ServiceHunter serviceHunter;
    private Agent.OnCFPListener onCFPListener;
    private InetAddress groupOwner;
    private int servicePort;
    private boolean p2pEnabled = false;
    private final ConcurrentHashMap<InetAddress, Integer> addressMap = new ConcurrentHashMap<>();
    private final LinkedBlockingQueue<WifiP2pDevice> connectionQueue = new LinkedBlockingQueue<>();
    private final HashMap<String, Map<String, String>> serviceLookupTable = new HashMap<>();
    private final IntentFilter intentFilter = new IntentFilter();


    public WifiDirectAgent(Context context, Agent.OnCFPListener listener) {
        this.intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        this.intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

        this.context = context;
        this.onCFPListener = listener;
        this.manager = (WifiP2pManager)context.getSystemService(Context.WIFI_P2P_SERVICE);
        this.channel = manager.initialize(context, context.getMainLooper(), null);

        try {
            this.networkManager = new NetworkManager(context, addressMap, onCFPListener, 10);
            this.serverExec = Executors.newSingleThreadExecutor();
            serverExec.execute(networkManager);
            this.servicePort = networkManager.getSocketPort();
        } catch (IOException ex) {
            Log.e(TAG, "Failed to create networkManager due to IOException");
            ex.printStackTrace();
        }
        this.serviceHunter = new ServiceHunter(manager, channel);

        this.wDirectReceiver = new WDirectReceiver(manager, channel, new WifiEventListener() {

            @Override
            public void onStateChanged(int reason) {
                if(reason == WifiP2pManager.WIFI_P2P_STATE_ENABLED){
                    p2pEnabled = true;
                    Log.d(TAG, "P2P state changed, P2P is enabled");
                } else {
                    p2pEnabled = false;
                    connectionQueue.clear();
                    serviceLookupTable.clear();
                    serviceHunter.stopDiscovery();
                    Log.d(TAG, "P2P state changed, P2P is disabled");
                }
            }

            @Override
            public void onConnectionChanged(boolean connected) {
                if(connected) {
                    Log.d(TAG, "Connected");
                    manager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener() {
                        @Override
                        public void onGroupInfoAvailable(final WifiP2pGroup group) {
                            if(!group.isGroupOwner()) {
                                serviceHunter.stopDiscovery();
                                Map<String, String> ownerMap = serviceLookupTable.get(group.getOwner().deviceAddress);
                                if(ownerMap != null) {
                                    final int ownerPort = Integer.parseInt(ownerMap.get(ServiceHunter.KEY_PORT));
                                    manager.requestConnectionInfo(channel, new WifiP2pManager.ConnectionInfoListener() {
                                        @Override
                                        public void onConnectionInfoAvailable(WifiP2pInfo info) {
                                            groupOwner = info.groupOwnerAddress;
                                            addressMap.putIfAbsent(info.groupOwnerAddress, ownerPort);
                                            Log.d(TAG, "Port number: " + String.valueOf(ownerPort));
                                            Bundle data = new Bundle();
                                            data.putInt("port", ownerPort);
                                            data.putString("address", groupOwner.getHostAddress());
                                            Message regmsg = Message.obtain(networkManager.handler, Agent.CODE_REG);
                                            regmsg.setData(data);
                                            if(networkManager.handler == null) {
                                                Log.d(TAG, "Handler is null...");
                                            }
                                            networkManager.handler.sendMessage(regmsg);
                                        }
                                    });
                                }
                                connectionQueue.clear();
                                serviceLookupTable.clear();
                            } else {
                                //Keep reaching out to other devices
                                if(!connectionQueue.isEmpty()) {
                                    try {
                                        connect(connectionQueue.poll(200, TimeUnit.MILLISECONDS));
                                    } catch(InterruptedException iEx) {
                                        //Swallow it
                                        Log.e(TAG, "Polling the connectionQueue was interrupted");
                                    }
                                }
                            }
                        }
                    });
                } else {
                    Log.d(TAG, "Disconnected");
                    connectionQueue.clear();
                    serviceLookupTable.clear();
                    serviceHunter.resetDiscovery(connectionQueue, serviceLookupTable);
                }

            }
        });

        serviceHunter.registerService(servicePort, "Anonymous");
        context.registerReceiver(wDirectReceiver, intentFilter);
    }

    private void connect(WifiP2pDevice device) {
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.wps.setup = WpsInfo.PBC;

        manager.connect(channel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Connecting...");
            }

            @Override
            public void onFailure(int reason) {
                Log.e(TAG, "Connection failed");
            }
        });
    }

    public void doConnect() {
        manager.cancelConnect(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Connection cancelled");
            }

            @Override
            public void onFailure(int reason) {
                Log.e(TAG, "Connection cancel failure");
            }
        });
        if(!connectionQueue.isEmpty()) {
            try {
                connect(connectionQueue.poll(100, TimeUnit.MILLISECONDS));
            } catch(InterruptedException iEx) {
                //Swallow it
                Log.e(TAG, "Polling the connectionQueue was interrupted");
            }
        }
    }

    public void doDisconnect() {
        if(manager != null && channel != null) {
            manager.cancelConnect(channel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "Connection cancelled");
                }

                @Override
                public void onFailure(int reason) {
                    Log.e(TAG, "Connection cancel failure");
                }
            });
            manager.removeGroup(channel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "Successfully disconnected");
                }

                @Override
                public void onFailure(int reason) {
                    Log.e(TAG, "Failed to disconnect");
                }
            });
        }
    }

    public void onDestroy() {
        if(manager != null && channel != null) {
            manager.cancelConnect(channel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "Connection cancelled");
                }

                @Override
                public void onFailure(int reason) {
                    Log.e(TAG, "Connection cancel failure");
                }
            });
            manager.removeGroup(channel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "Successfully disconnected");
                }

                @Override
                public void onFailure(int reason) {
                    Log.e(TAG, "Failed to disconnect");
                }
            });
            manager.clearServiceRequests(channel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "Service requests cleared");
                }

                @Override
                public void onFailure(int reason) {
                    Log.e(TAG, "Service requests not cleared");
                }
            });
            serviceHunter.unregisterService();
            context.unregisterReceiver(wDirectReceiver);
            networkManager.cleanup(5);
            serverExec.shutdown();
            try {
                if (!serverExec.awaitTermination(10, TimeUnit.SECONDS)) {
                    serverExec.shutdownNow();
                }
            } catch (InterruptedException ex) {
                Log.e(TAG, "Pool shutdown was interrupted");
                serverExec.shutdownNow();
            }
        }
    }

    @Override
    public void callForProposals(final HashMap<String, String> data, final BidListener bidListener) {
        if(!p2pEnabled) {
            bidListener.onReject();
        } else {
            //TODO: change this to msg
            networkManager.setMasterBidListener(bidListener);
            Message cfpMsg = Message.obtain(networkManager.handler, Agent.CODE_CFP, 5, 0, data);
            networkManager.handler.sendMessage(cfpMsg);
        }
    }
}
