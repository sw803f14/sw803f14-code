package dk.wukong.sunwukong.agent_lib.agent_interface;

import dk.wukong.sunwukong.agent_lib.networking.ClientMessenger;

/**
 * Created by nattfari on 5/18/14.
 */
public interface ClientBidListener {
    void onBid(int cost, ClientMessenger messenger);
    void onReject();
}
