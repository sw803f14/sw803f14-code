package dk.wukong.sunwukong.agent_lib.agent_interface;

/**
 * Created by nattfari on 5/12/14.
 */
public interface WifiEventListener {
    void onStateChanged(int reason);
    void onConnectionChanged(boolean connected);
}
