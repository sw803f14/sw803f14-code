package dk.wukong.sunwukong.agent_lib.networking;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Pair;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


import dk.wukong.sunwukong.agent_lib.agent_interface.Agent;
import dk.wukong.sunwukong.agent_lib.agent_interface.ClientBidListener;

/**
 * Created by nattfari and vincent on 5/15/14.
 */
public class NetworkManager implements Runnable{
    private static final String TAG = "NetworkManager";

    private ServerSocket socket;
    private ExecutorService pool;
    private ExecutorService messengerPool;
    private ScheduledExecutorService reporter;
    private Context context;
    private Agent.OnCFPListener cfpListener;
    private Agent.BidListener masterBidListener;
    private int socketPort = -1;
    private ConcurrentHashMap<InetAddress, Integer> addressMap;
    private final ConcurrentSkipListMap<Integer, ClientMessenger> bids = new ConcurrentSkipListMap<>();
    private InetAddress ownAddress = null;

    public Handler handler;

    public NetworkManager(Context cont, ConcurrentHashMap<InetAddress, Integer> addresses, Agent.OnCFPListener listener, int poolSize) throws IOException {
        socket = new ServerSocket(0);
        this.context = cont;
        this.addressMap = addresses;
        this.cfpListener = listener;
        this.socketPort = socket.getLocalPort();
        this.pool = Executors.newFixedThreadPool(poolSize);
        this.messengerPool = Executors.newFixedThreadPool(poolSize);
        this.reporter = Executors.newSingleThreadScheduledExecutor();

        Log.d(TAG, "TCPServer created with Socket: " + String.valueOf(socketPort));
    }

    public void setMasterBidListener(Agent.BidListener listener) {
        this.masterBidListener = listener;
    }

    public int getSocketPort() {
        return this.socketPort;
    }

    private void reportBids() {
        Log.d(TAG, "Reporting bids");
        if(masterBidListener != null) {
            if(!bids.isEmpty()) {
                int cost = bids.firstEntry().getKey();
                ClientMessenger lowestBidder = bids.firstEntry().getValue();
                bids.remove(cost, lowestBidder);
                for(ClientMessenger messenger : bids.values()) {
                    messenger.reject();
                }
                masterBidListener.onBid(cost, lowestBidder);
            } else {
                masterBidListener.onReject();
            }
        }
    }

    private void broadcastCFP(HashMap<String, String> data, int timeout) {
        Log.d(TAG, "Broadcasting CFP");
        bids.clear();
        for(Map.Entry<InetAddress, Integer> entry : addressMap.entrySet()) {
            sendCFP(entry.getKey(), entry.getValue(), data, new ClientBidListener() {

                @Override
                public void onBid(int cost, ClientMessenger messenger) {
                    bids.putIfAbsent(cost, messenger);
                }

                @Override
                public void onReject() {
                }
            });
        }
        reporter.schedule(new Runnable() {
            @Override
            public void run() {
                reportBids();
            }
        }, timeout, TimeUnit.SECONDS);
    }

    private void sendCFP(InetAddress address, int port, HashMap<String, String> data, ClientBidListener listener) {
        Log.d(TAG, "Sending CFP");
        Socket msgSocket;
        ClientRunnable clientRunnable;
        try {
            msgSocket = new Socket(address, port);
            clientRunnable = new ClientRunnable(msgSocket, this);
            messengerPool.execute(clientRunnable);
            clientRunnable.setBidListener(listener);
            Message cfpMsg = Message.obtain(handler, Agent.CODE_CFP, data);
            clientRunnable.handler.sendMessage(cfpMsg);

        } catch(IOException ex) {
            Log.e(TAG, "Sending message failed due to socket IOException");
            addressMap.remove(address, port);
        }
    }

    public void registerPeerService(InetAddress addr, Integer port) {
        Log.d(TAG, "Registering a service");
        addressMap.putIfAbsent(addr, port);
    }

    private void register(InetAddress address, int port) {
        Socket msgSocket;
        ClientRunnable clientRunnable;
        try {
            msgSocket = new Socket(address, port);
            clientRunnable = new ClientRunnable(msgSocket, this);
            messengerPool.execute(clientRunnable);
            Message regMsg = Message.obtain(handler, Agent.CODE_REG);
            clientRunnable.handler.sendMessage(regMsg);
        } catch (IOException ex) {
            Log.e(TAG, "Registering failed due to IOException");
            ex.printStackTrace();
        }
    }

    public ConcurrentHashMap<InetAddress, Integer> getAddressMap() {
        return addressMap;
    }

    public void updateAddressMap(ConcurrentHashMap<InetAddress, Integer> data) {
        addressMap = data;
        if(ownAddress == null) {
            ownAddress = getOwnAddress(context);
        }
        addressMap.remove(ownAddress);
    }

    @Override
    public void run() {
        try {
            Looper.prepare();
            handler = new Handler() {
                public void handleMessage(Message msg) {
                    int what = msg.what;
                    switch (what) {
                        case(Agent.CODE_CFP):
                            //CFP
                            int timeout = msg.arg1;
                            HashMap<String, String> data = (HashMap<String, String>) msg.obj;
                            broadcastCFP(data, timeout);
                            break;
                        case(Agent.CODE_REG):
                            //Register
                            Bundle incoming = msg.getData();
                            try {
                                InetAddress address = InetAddress.getByName(incoming.getString("address"));
                                int port = incoming.getInt("port");
                                register(address, port);
                            } catch (UnknownHostException ex) {
                                ex.printStackTrace();
                            }
                            break;
                        case(Agent.CODE_REQ):
                            //Request addresses
                            break;
                        default:
                            //Message not understood
                            break;
                    }
                }
            };
            Looper.loop();
            while(!Thread.currentThread().isInterrupted()) {
                pool.execute(new ServerRunnable(socket.accept(), this, cfpListener));
            }
        } catch(IOException ex) {
            Log.e(TAG, "ServerRunnable could not be run due to IOException");
        } finally {
            Looper.myLooper().quit();
            cleanup(5);
        }
    }

    public void cleanup(long seconds) {
        pool.shutdown();
        messengerPool.shutdown();
        reporter.shutdown();
        try {
            if (!pool.awaitTermination(seconds, TimeUnit.SECONDS)) {
                pool.shutdownNow();
            }
            if(!messengerPool.awaitTermination(seconds, TimeUnit.SECONDS)) {
                messengerPool.shutdownNow();
            }
            if(!reporter.awaitTermination(seconds, TimeUnit.SECONDS)) {
                reporter.shutdownNow();
            }
        } catch (InterruptedException ex) {
            Log.e(TAG, "Pool shutdown was interrupted");
        }
    }

    private InetAddress getOwnAddress(Context context) {
        int ownAddress = -1;
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifiManager.getDhcpInfo();
        if(dhcp == null) {
            return null;
        }
        ownAddress = dhcp.ipAddress;
        byte[] quads = new byte[4];
        for(int j = 0; j < 4; j++) {
            quads[j] = (byte) (ownAddress >> (j*8));
        }
        try {
            return InetAddress.getByAddress(quads);
        } catch (UnknownHostException ex) {
            Log.e(TAG, "UnkownHostException occurred when trying to fetch broadcast address");
            return null;
        }
    }
}
