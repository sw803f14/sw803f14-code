package dk.wukong.sunwukong.agent_lib.networking;

import android.os.Handler;
import android.os.Message;

import dk.wukong.sunwukong.agent_lib.agent_interface.Agent;

/**
 * Created by nattfari on 5/24/14.
 */
public class ServerMessenger implements Agent.Server{
    private Handler handler;
    private ServerRunnable runnable;

    public ServerMessenger(Handler h, ServerRunnable r) {
        this.handler = h;
        this.runnable = r;
    }

    @Override
    public void bid(int cost, Agent.ApprovalListener approvalListener) {
        runnable.setApprovalListener(approvalListener);
        Message bidMsg = Message.obtain(handler, Agent.CODE_BID, cost);
        handler.sendMessage(bidMsg);
    }

    @Override
    public void reject() {
        Message rejectMsg = Message.obtain(handler, Agent.CODE_REJECT);
        handler.sendMessage(rejectMsg);
    }
}
