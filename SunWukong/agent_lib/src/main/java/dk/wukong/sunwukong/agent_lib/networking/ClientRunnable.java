package dk.wukong.sunwukong.agent_lib.networking;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import dk.wukong.sunwukong.agent_lib.agent_interface.Agent;
import dk.wukong.sunwukong.agent_lib.agent_interface.ClientBidListener;

/**
 * Created by nattfari on 5/16/14.
 */
public class ClientRunnable implements Runnable {

    private static final String TAG = "ClientRunnable: ";
    private Socket socket;
    private NetworkManager networkManager;
    private ClientBidListener bidListener;
    private Agent.TransferListener transferListener;
    private DataInputStream inStream;
    private PrintWriter outStream;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;
    public Handler handler;

    public ClientRunnable(Socket tcpSock, NetworkManager server) {
        this.socket = tcpSock;
        this.networkManager = server;
    }

    public void setBidListener(ClientBidListener listener) {
        this.bidListener = listener;
    }

    public void setTransferListener(Agent.TransferListener listener) {
        this.transferListener = listener;
    }

    private String readLine(DataInputStream in) {
        StringBuffer buffer = new StringBuffer();
        String ret = null;
        try {
            char next = in.readChar();
            while(next != '\n' && next != '\r') {
                buffer.append(next);
                next = in.readChar();
            }
            ret = buffer.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return ret;
    }

    private void callForProposals(Map<String, String> data, ClientBidListener bidListener) {
        try {
            this.bidListener = bidListener;
            outStream.println(Agent.MSG_CFP);
            objectOutputStream.reset();
            objectOutputStream.writeObject(data);
            objectOutputStream.flush();
        } catch (IOException ex) {
            Log.e(TAG, "CFP failed, see stacktrace for details");
            ex.printStackTrace();
        }
    }

    private void accept(Agent.TransferListener transferListener) {
        this.transferListener = transferListener;
        outStream.println(Agent.MSG_ACCEPT);
    }

    private void reject() {
        outStream.println(Agent.MSG_REJECT);
        Thread.currentThread().interrupt();
    }

    private void register() {
        outStream.println(Agent.MSG_REG);
        Thread.currentThread().interrupt();
    }

    private void requestAddressList() {
        outStream.println(Agent.MSG_REQ);
        try {
            ConcurrentHashMap<InetAddress, Integer> inputMap = (ConcurrentHashMap<InetAddress, Integer>) objectInputStream.readObject();
            networkManager.updateAddressMap(inputMap);
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        Log.d(TAG, "ClientRunnable is now running");
        try {
            Looper.prepare();
            handler = new Handler() {
                public void handleMessage(Message msg) {
                    int what = msg.what;
                    switch (what) {
                        case(Agent.CODE_ACCEPT):
                            //Accept
                            accept(transferListener);
                            break;
                        case(Agent.CODE_REJECT):
                            //Reject
                            reject();
                            break;
                        case(Agent.CODE_CFP):
                            //CFP
                            HashMap<String, String> data = (HashMap<String, String>) msg.obj;
                            callForProposals(data, bidListener);
                            break;
                        case(Agent.CODE_REG):
                            //Register
                            register();
                            break;
                        case(Agent.CODE_REQ):
                            //Request addresses
                            requestAddressList();
                            break;
                        default:
                            //Message not understood
                            break;
                    }
                }
            };
            Looper.loop();
            inStream = new DataInputStream(socket.getInputStream());
            outStream = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectInputStream = new ObjectInputStream(socket.getInputStream());

            while (!Thread.currentThread().isInterrupted()) {
                String input = readLine(inStream);
                String output = protocol(input);
                Log.d(TAG, "ClientOutput: " + output);
            }

        } catch (IOException ex) {
            Log.e(TAG, "Streams could not be opened");
            ex.printStackTrace();
        } finally {
            Looper.myLooper().quit();
            try {
                if(inStream != null) {
                    inStream.close();
                }
                if(outStream != null) {
                    outStream.close();
                }
                if(socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                Log.e(TAG, "Problem closing streams and socket");
                ex.printStackTrace();
            }

        }
    }

    private String protocol(String input) {
        String output = input;

        switch (input) {
            case(Agent.MSG_BID):
                Log.d(TAG, "Bid gotten");
                //TODO: Send to service (append to bid list or some?)
                String stringCost = readLine(inStream);
                if(bidListener != null) {
                    int cost = 9999;
                    try {
                        cost = Integer.parseInt(stringCost);
                    } catch (NumberFormatException ex) {
                        Log.d(TAG, "Bid was not followed by a cost");
                    }

                    bidListener.onBid(cost, new ClientMessenger(handler, this));
                }
                break;
            case(Agent.MSG_REJECT):
                Log.d(TAG, "Reject gotten");
                //TODO: Send to service
                if(bidListener != null) {
                    bidListener.onReject();
                }
                //Break the loop and disconnect
                Thread.currentThread().interrupt();
                break;
            default:
                //Erronous
                Log.d(TAG, "Invalid input gotten");
                break;
        }

        return output;
    }
}
