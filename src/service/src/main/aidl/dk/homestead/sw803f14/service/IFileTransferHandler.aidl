package dk.homestead.sw803f14.service;

interface IFileTransferHandler {
    /**
     * Called before any bytes are transferred. Allows the recipient to prep data structures if
     * needed.
     * @return If false is returned, the operation will be cancelled.
     **/
    boolean onBegin();

    /**
     * Called repeatedly with an array of bytes from the fileId being transferred. A new array of
     * bytes will not be sent until any current onByte method returns.
     * @return if false is returned, the opreation will be cancelled.
     **/
    void onByte(in byte[] bytes);

    /**
     * Called when there are no more bytes to transfer, signifying the copmletion of the fileId (EOF
     * reached).
     **/
    void onComplete();
}