package dk.homestead.sw803f14.service;

import dk.homestead.sw803f14.service.SunWukongClient;
import dk.homestead.sw803f14.service.SunWukongDomain;
import dk.homestead.sw803f14.service.FileInfo;
import dk.homestead.sw803f14.service.StringTriplet;

/**
 * The ISunWukongClient AIDL interface is the targetClient-side interface the service uses to respond to,
 * or make requests from, a targetClient.
 **/
interface ISunWukongClient {
    /**
     * Called after successful registration with the service. Contains the ClientKey which *must*
     * be used in all app-specific requests from then on (in other words, don't lose it!).
     **/
    void onClientRegistered(in SunWukongClient.ClientKey clientKey);

    void onClientUnregistered();

    /**
     * Called when a targetClient's request to join a domain succeeds. The attached DomainKey must be
     * used when ambiguity due to domains may occur.
     **/
    void onDomainJoined(in SunWukongDomain.DomainKey key);

    /**
     * Called when a targetClient's request to leave a domain is fulfilled. The key is included in case
     * the targetClient has joined several domains and need to distinguish one leave from another.
     **/
    void onDomainLeft(in SunWukongDomain.DomainKey key);

    /**
     * Called when the service is in need of a specific fileId. It is assumed that this fileId exists
     * (for example after a call to onGetFileInfo). The targetClient should return a fileId descriptor for
     * the requested fileId.
     * The service will copy the fileId to its own storage and close the handler.
     * @param fileId Unique fileId identifier of the fileId to retrieve.
     **/
    ParcelFileDescriptor onOpenFile(in SunWukongDomain.DomainKey domain, String fileId);

    /**
     * Called when the service requires fileId information from a targetClient. The targetClient must construct
     * and return a valid FileInfo object to the service if the fileId is known, null otherwise.
     **/
    FileInfo onGetFileInfo(in SunWukongDomain.DomainKey domain, String fileId);

    /**
     * Called when the service is looking for a fileId among its clients. The targetClient must construct
     * and return a valid FileInfo object to the service if the fileId is known, null otherwise.
     **/
    FileInfo onFindFileInfo(in SunWukongDomain.DomainKey domain, in StringTriplet[] criteria);
}