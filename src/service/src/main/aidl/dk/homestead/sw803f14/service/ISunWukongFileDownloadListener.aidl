package dk.homestead.sw803f14.service;

interface ISunWukongFileDownloadListener {
    /**
     * Called regularly with intervals to keep the targetClient appraised of download progress. Interval
     * cannot be guaranteed, as the service must accomodate possible system strain and the like.
     **/
    void onProgress(long current, long total);

    /**
     * Called every time the status of the download changes.
     * @param newStatus The new status, an integer representation (ordinal) from DownloadStatus.
     **/
    void onStatus(String newStatus);

    /**
     * Called when the download is complete and available in the fileId system.
     **/
    void onCompleted();

    /**
     * Called if the transfer failed definitely for any reason. This happens *after* the status
     * change is reported.
     * @param errorMsg Contains a human-readable explanation of the error.
     **/
    void onFailed(String errorMsg);
}