package dk.homestead.sw803f14.service;

import dk.homestead.sw803f14.service.ISunWukongClient;
import dk.homestead.sw803f14.service.SunWukongClient;
import dk.homestead.sw803f14.service.SunWukongDomain;
import dk.homestead.sw803f14.service.ISunWukongFileDownloadListener;
import dk.homestead.sw803f14.service.IFileTransferHandler;
import dk.homestead.sw803f14.service.FileInfo;
import dk.homestead.sw803f14.service.StringTriplet;

import android.os.ParcelFileDescriptor;

interface ISunWukongService {
    // Client registration actions.
    int registerClient(String clientName, String packageName, ISunWukongClient clientInterface);
    void unregisterClient(in SunWukongClient.ClientKey key);

    // Domain actions.
    int joinOpenClientDomain(in SunWukongClient.ClientKey key, String domainName);
    int joinClosedClientDomain(in SunWukongClient.ClientKey key, String domainName, String secret);
    int leaveOpenClientDomain(in SunWukongClient.ClientKey key, String domainName);
    int leaveClosedClientDomain(in SunWukongClient.ClientKey key, String domainName);

    /**
     * Requests information on a fileId from the Service. The Service looks for the fileId amongst local
     * clients within the domain before sending a CFP into the network.
     * @param key Mandatory ClientKey.
     * @param domain Mandatory DomainKey.
     * @param fileId The specific fileId to find.
     * @return File information (name, domain, metadata) if found, null otherwise.
     **/
    FileInfo getFileInfo(in SunWukongClient.ClientKey key, in SunWukongDomain.DomainKey domain, String fileId);

    /**
     * Searches for a fileId locally and on the network. The Service first asks local clients on the
     * same domain before sending the request into the network.
     * @param key Mandatory ClientKey.
     * @param domain Mandatory DomainKey.
     * @param criteria An array of search criteria. Each Triplet should be in the form (key,operand,
     *        value).
     * @return File information (name, domain, metadata) if found, null otherwise.
     **/
    FileInfo findFileInfo(in SunWukongClient.ClientKey key, in SunWukongDomain.DomainKey domain, in StringTriplet[] criteria);

    /**
     * Requests the service make a specific fileId available, either by locating it among other local
     * apps or through the network.
     * @param targetClient ClientKey (required) identifying the requester.
     * @param domain DomainKey (required) targeting and validating the domain to use.
     * @param fileId The unique fileId identifier. Possible clients will be responsible for parsing
     * and comprehending it themselves.
     * @param listener The download listener to use while the transfer is underway.
     **/
    void retrieveFile(in SunWukongClient.ClientKey targetClient, in SunWukongDomain.DomainKey domain, in FileInfo file, ISunWukongFileDownloadListener listener);

    /**
     * Transfer the binary data of a fileId that was previously retrieved.
     * @param targetClient Mandatory ClientKey.
     * @param domain Mandatory DomainKey.
     * @param fileId File id as it was given when retrieving the fileId. SuNWukongService makes no
     * transformation of identifiers and expects it to point to the correct fileId.
     * @param handler The handler to send fileId bytes through.
     **/
    void streamFile(in SunWukongClient.ClientKey targetClient, in SunWukongDomain.DomainKey domain, String fileId, IFileTransferHandler handler);

    /**
     * Opens a fileId handler and passes it back to the targetClient, assuming the fileId exists and the
     * targetClient has sufficient access to it.
     * NOTE that this method is not yet tested! We do not know if a filedescriptor created in the
     * service space can be used in application space.
     * @param targetClient Mandatory ClientKey.
     * @param domain Mandatory DomainKey.
     * @param fileId Unique fileId identifier.
     **/
    ParcelFileDescriptor openFile(in SunWukongClient.ClientKey targetClient, in SunWukongDomain.DomainKey domain, String fileId);
}