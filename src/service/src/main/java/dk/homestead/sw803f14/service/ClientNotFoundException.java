package dk.homestead.sw803f14.service;

/**
 * Thrown when a sought targetClient is not found.
 * Created by lindhart on 5/9/14.
 */
public class ClientNotFoundException extends Exception {
    public ClientNotFoundException(String msg) {
        super(msg);
    }
}
