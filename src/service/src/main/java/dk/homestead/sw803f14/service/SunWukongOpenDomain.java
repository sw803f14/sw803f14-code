package dk.homestead.sw803f14.service;

/**
 * The open domain is a flat extension of the base domain class, merely to separate it laterally
 * from the closed domain type.
 */
public class SunWukongOpenDomain extends SunWukongDomain {
    public SunWukongOpenDomain(String domainName) {
        super(domainName);
    }

    @Override
    public boolean isOpen() {
        return true;
    }
}
