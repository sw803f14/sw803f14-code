package dk.homestead.sw803f14.service;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * A domain is a shared set of fileId blocks. In order to retrieve a given data block, a targetClient must
 * be a registered member of the domain (supplying the shared secret key).
 * Created by lindhart on 5/7/14.
 */
public abstract class SunWukongDomain {
    private final String domainName;
    private final String privateKey;
    protected Set<SunWukongClient.ClientKey> clients = new HashSet<>();

    public SunWukongDomain(String domainName) {
        this.domainName = domainName;
        privateKey = RandomString.generate(32);
    }

    public String domainName() { return domainName; }

    public void removeClient(SunWukongClient.ClientKey client) {
        clients.remove(client);
    }
    public void addClient(SunWukongClient.ClientKey client) { clients.add(client); }

    /**
     * Returns an unmodifiable list of clients currently in the domain.
     * @return Unmodifiable list of clients.
     */
    public Set<SunWukongClient.ClientKey> getClients() {
        return Collections.unmodifiableSet(clients);
    }

    /**
     * Gives information in regards to whether this domain is open or closed.
     * @return True if this is an open domain, false otherwise.
     */
    public abstract boolean isOpen();

    protected DomainKey getDomainKey() {
        return new DomainKey(domainName(), privateKey);
    }

    protected boolean acceptDomainKey(DomainKey key) {
        return key.privateKey.equals(privateKey);
    }

    /* WIP code, an attempt to increase domain security.
    protected String generateKeyPair() {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        keyGen.initialize(1024, random);
    }
    */

    /**
     * DomainKey is for domains what DomainKey is to clients - that is, it is a unique, non-cloneable
     * key to a domain that clients can use to access that domain's files.
     * Created by lindhart on 5/13/14.
     */
    public static class DomainKey implements Parcelable {
        static Parcelable.Creator<DomainKey> CREATOR = new Parcelable.Creator<DomainKey>() {
            @Override
            public DomainKey createFromParcel(Parcel parcel) {
                return new DomainKey(parcel.readString(), parcel.readInt()!=0);
            }

            @Override
            public DomainKey[] newArray(int i) {
                return new DomainKey[0];
            }
        };

        private final String domainName;
        private final boolean openDomain;
        private final String privateKey;

        private DomainKey(String name, boolean open) {
            domainName = name;
            privateKey = "";
            openDomain = true;
        }

        private DomainKey(String name, String pKey) {
            domainName = name;
            privateKey = pKey;
            openDomain = false;
        }

        public String domainName() {
            return domainName;
        }

        public boolean isOpen() {
            return openDomain;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(domainName());
            parcel.writeInt(openDomain ? 1 : 0);
        }
    }
}
