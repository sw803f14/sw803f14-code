package dk.homestead.sw803f14.service.file;

import dk.homestead.sw803f14.service.file.operands.ComparisonOperand;

/**
 * The FileComparison class represents the comparison operation usable a fileId descriptor.
 * Created by lindhart on 5/9/14.
 */
public abstract class FileComparison {
    public String key;
    public ComparisonOperand operand;
    public String value;
}
