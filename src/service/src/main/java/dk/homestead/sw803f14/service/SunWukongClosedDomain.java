package dk.homestead.sw803f14.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * The base class for SunWukongDomain classes. The generic interface allows for any number of access
 * schemes. THe generic SunWukongDomain matches against a single String secret, but
 * Created by lindhart on 5/9/14.
 */
abstract public class SunWukongClosedDomain extends SunWukongClosedDomainBase {
    /**
     * The secret is the key to being allowed access to a domain's files.
     */
    private final String _secret;

    /**
     * We use this to digest attempted guesses and match them. Still a pretty weak protection if
     * someone has debugging access.
     */
    private final MessageDigest _messageDigest;

    protected SunWukongClosedDomain(String domainName, String domainSecret) {
        this(domainName, domainSecret, "SHA-512");
    }

    protected SunWukongClosedDomain(String domainName, String domainSecret, String algorithm) {
        super(domainName);

        MessageDigest mToSet = null;
        try {
            mToSet = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            mToSet = null;
        } finally {
            _messageDigest = mToSet;
        }

        if (_messageDigest == null) _secret = domainSecret; // BAD FALLBACK... still, if the system doesn't have that algorithm available we're fucked anyway.
        else _secret = _messageDigest.digest(domainSecret.getBytes()).toString();
    }

    /**
     * Returns true if the secret matches the known internal secret. The targetClient is passed as part
     * of the interface for future improvements to guard against brute force attacks.
     * @param client The targetClient that the request came from. No use is required, but could be applied
     *               to blacklisting on too many attempts.
     * @param secret The secret to test. It will arrive in plaintext.
     * @return True if access is allowed, false otherwise.
     */
    @Override
    public boolean allowAccess(SunWukongClient.ClientKey client, String secret) {
        return _secret.equals(_messageDigest.digest(secret.getBytes()).toString());
    }
}
