package dk.homestead.sw803f14.service;

/**
 * Exception thrown when a Domain is sought and not found.
 * Created by lindhart on 5/13/14.
 */
public class DomainNotFoundException extends Exception {
    public DomainNotFoundException(String msg) {
        super(msg);
    }
}
