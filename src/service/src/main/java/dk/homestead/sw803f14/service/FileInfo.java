package dk.homestead.sw803f14.service;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A fileId descriptor describes a fileId, either locally available or over the network, along with its
 * metadata. For the latter reason, the fileId descriptor uses a hashmap for its metadata table over
 * regular fields.
 * Subclasses are recommended to establish a stricter metadata schema, either through
 * fields/accessors or simply table definition in the constructor.
 * Created by lindhart on 5/9/14.
 */
public class FileInfo implements Parcelable {

    protected final String filename;

    /**
     * The domain that the fileId belongs to. This information can be made publicly available as only
     * clients already part of the domain will receive the info or the request.
     */
    protected SunWukongDomain.DomainKey domain;

    /**
     * The targetClient known to have the fileId.
     * THIS INFORMATION MUST NEVER BE AVAILABLE OUTSIDE THE PACKAGE AS IT MAY ALLOW A MALICIOUS
     * CLIENT TO IMPERSONATE OTHER CLIENTS.
     */
    private SunWukongClient.ClientKey sourceClient;

    public static Creator<FileInfo> CREATOR = new Creator<FileInfo>() {
        /**
         * Creates a FileDescriptor from a compatible parcel. The parcel structure is as follows:
         * - Filename
         * - Domain name
         * - Metatable string separator (auto-generated)
         * - Metatable (as a List of Strings, to be parsed using the separator).
         * @param parcel The Parcel to construct a FileDescriptor from.
         * @return A new FileDescriptor, matching the passed parcel.
         */
        @Override
        public FileInfo createFromParcel(Parcel parcel) {
            String fileName = parcel.readString();
            SunWukongDomain.DomainKey dKey = parcel.readParcelable(SunWukongDomain.DomainKey.class.getClassLoader());
            FileInfo retVal = new FileInfo(fileName, dKey);

            String metatable_separator = parcel.readString();

            List<String> metatable_list = new ArrayList<>();
            parcel.readStringList(metatable_list);

            // Variable for the loop.
            String[] table_entry;
            for (String entry : metatable_list) {
                table_entry = entry.split(metatable_separator);
                retVal.setMetadata(table_entry[0], table_entry[1]);
            }

            return retVal;
        }

        @Override
        public FileInfo[] newArray(int i) {
            return new FileInfo[i];
        }
    };

    private HashMap<String, String> metadata = new HashMap<>();
    public void setMetadata(String key, String value) {
        metadata.put(key, value);
    }
    public String getMetadata(String key) {
        return metadata.get(key);
    }

    public FileInfo(String filename, SunWukongDomain.DomainKey domain) {
        this.filename = filename;
    }

    public String filename() {
        return filename;
    }
    public SunWukongDomain.DomainKey getDomainKey() { return domain; }
    protected SunWukongClient.ClientKey getSourceClient() { return sourceClient; }
    protected void setSourceClient(SunWukongClient.ClientKey key) { sourceClient = key; }

    /**
     * Calculate and return a valid separator. The separator is guaranteed to be a unique String,
     * able to delineate the barrier between key/value pairs when serialised.
     * The return value *may change* between calls to setMetadata!
     * NOTE: The algorithm generating separators is both simplistic and non-exhaustive. While it
     * will never run indefinitely, it *can* cause arbitrarily large separators in edge cases.
     * @return A separator *unique to this instance and moment*.
     */
    protected final String getSeparator() {
        String retVal = ";";

        while (metadata.containsKey(retVal) || metadata.containsValue(retVal)) retVal += ";";

        return retVal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(filename());
        parcel.writeParcelable(getDomainKey(), PARCELABLE_WRITE_RETURN_VALUE);

        String separator = getSeparator();
        parcel.writeString(separator);

        List<String> metatable = new ArrayList<>();

        for (Map.Entry<String,String> entry : metadata.entrySet()) {
            metatable.add(entry.getKey() + separator + entry.getValue());
        }

        parcel.writeList(metatable);
    }
}
