package dk.homestead.sw803f14.service;

/**
 * Thrown when a targetClient is already in a domain it wants to join.
 * Created by lindhart on 5/9/14.
 */
public class ClientAlreadyInDomainException extends Exception {
    public ClientAlreadyInDomainException(String msg) {
        super(msg);
    }
}
