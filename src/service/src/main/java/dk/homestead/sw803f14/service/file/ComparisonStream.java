package dk.homestead.sw803f14.service.file;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dk.homestead.sw803f14.service.file.operands.ComparisonOperand;
import dk.homestead.sw803f14.service.file.operands.EqualsOperand;

/**
 * Created by lindhart on 5/13/14.
 * ComparisonStream can take a string and convert it to a collection of ComparisonOperands as well
 * as turn one or more ComparisonOperands into a String fit for sending over the network for
 * consumption by another class. Pretty simple serialisation stuff.
 */
public class ComparisonStream {
    public static List<ComparisonOperand> fromString(String val) {
        List<ComparisonOperand> retVal = new ArrayList<>();

        String regexPattern = "([A-Za-z0-9]+)(<|<=|>|>=|~|==|!=)([A-Za-z0-9]+);";
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(val);
        while (matcher.find()) {
            // Construct correct comparison operator for each match.
            // Middle group contains operator.
            retVal.add(comparisonOperandFromString(matcher.group(1), matcher.group(2), matcher.group(3)));
        }
        return retVal;
    }

    static ComparisonOperand comparisonOperandFromString(String key, String op, String val) {
        switch (op) {
            case "==": return new EqualsOperand(key, val);
            /*case "<=": return new LessThanOrEqualOperand(key, val);
            case "<": return new LessThanOperand(key, val);
            case ">=": return new GreaterThanOrEqualOperand(key, val);
            case ">": return new GreaterThanOperand(key, val);
            case "~": return new LikeOperand(key, val);
            case "!=": return new NotEqualOperand(key, val);*/
            default: return null;
        }
    }
}
