package dk.homestead.sw803f14.service;

/**
 * The base class for SunWukongDomain classes. The generic interface allows for any number of access
 * schemes. THe generic SunWukongDomain matches against a single String secret, but
 * Created by lindhart on 5/9/14.
 */
abstract public class SunWukongClosedDomainBase extends SunWukongDomain {
    protected SunWukongClosedDomainBase(String domainName) {
        super(domainName);
    }

    /**
     * Returns true if the secret matches the known internal secret. The targetClient is passed as part
     * of the interface for future improvements to guard against brute force attacks.
     * @param client The targetClient that the request came from. No use is required, but could be applied
     *               to blacklisting on too many attempts.
     * @param secret The secret to test. It will arrive in plaintext.
     * @return True if access is allowed, false otherwise.
     */
    public abstract boolean allowAccess(SunWukongClient.ClientKey client, String secret);

    public void addClient(SunWukongClient.ClientKey client, String secret) throws WrongDomainSecretException {
        if (allowAccess(client, secret)) clients.add(client);
        else throw new WrongDomainSecretException("The join request was denied.");
    }

    @Override
    public boolean isOpen() {
        return false;
    }
}
