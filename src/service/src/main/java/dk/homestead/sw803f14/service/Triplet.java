package dk.homestead.sw803f14.service;

/**
 * Created by lindhart on 5/19/14.
 */
public class Triplet<A,B,C> {
    public A left;
    public B center;
    public C right;

    public Triplet() {}

    public Triplet(A left, B center, C right) {
        this.left = left;
        this.center = center;
        this.right = right;
    }
}
