package dk.homestead.sw803f14.service.file;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import dk.homestead.sw803f14.service.SunWukongDomain;

/**
 * The FileIdentifier class is an example of how a targetClient might identify apps internally and towards
 * the network. A FileIdentifier will typically not cross interface barriers (AIDL) to save system
 * resources.
 * Created by lindhart on 5/9/14.
 */
public class FileIdentifier {

    private SunWukongDomain _domain;
    private MessageDigest _messageDigest;

    public FileIdentifier(SunWukongDomain domain) {
        _domain = domain;
        try {
            _messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            // We should never get here.
            e.printStackTrace();
        }
    }

    public String getIdentifier(String fileName) {
        String hashF = _messageDigest.digest(fileName.getBytes()).toString();
        String hashD = _messageDigest.digest(_domain.domainName().getBytes()).toString();
        return hashD+hashF;
    }
}
