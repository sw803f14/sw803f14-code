package dk.homestead.sw803f14.service.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.BaseColumns;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The SunWukongProvider is responsible for central, persistent tracking of registered clients and
 * their files. It is also responsible for serving downloaded files to clients.
 * The provider grants per-URI access to clients based on known registrations with domains.
 * Created by lindhart on 18-05-2014.
 */
public class SunWukongProvider extends ContentProvider {

    public static String AUTHORITY = "dk.homestead.sw803f14.provider";
    /**
     * Base URI for all access.
     */
    public static final Uri BASE_URI = Uri.parse("content://" + AUTHORITY + "/");

    /**
     * Content Uri for clients. Access is *never* granted to outside packages as a safety measure.
     */
    public static final Uri CLIENTS_URI = Uri.withAppendedPath(BASE_URI, "clients");

    /**
     * Content Uri for domains. Like clients, access is *never* granted to external code.
     */
    public static final Uri DOMAINS_URI = Uri.withAppendedPath(BASE_URI, "domains");

    /**
     * Content Uri for files. Access is granted to specific sub-Uri's on a case-by-case basis.
     */
    public static final Uri FILES_URI = Uri.withAppendedPath(BASE_URI, "files");

    public static final Uri makeFileAccessUri(String id, String clientPackage) {
        return Uri.withAppendedPath(BASE_URI, "files/" + id + "/grant/" + clientPackage);
    }

    /**
     * Debug tag.
     */
    private static final String TAG = "SunWukongProvider";

    /** Various database-related constants. **/
    private static final String DATABASE_NAME = "SunWukongService.db";
    private static final int DATABASE_VERSION = 1;
    private static final String FILES_TABLE = "files";
    private static final String DOMAINS_TABLE = "domains";
    private static final String CLIENTS_TABLE = "clients";
    private static final String CLIENTS_DOMAINS_TABLE = "client_domain_relationships";

    /** MIME TYPES **/
    public static final String DOMAIN_TYPE = "vnd.android.cursor.dir/dk.homestead.sw803f14.Domain";
    public static final String DOMAIN_ITEM_TYPE = "vnd.android.cursor.item/dk.homestead.sw803f14.Domain";
    public static final String CLIENT_TYPE = "vnd.android.cursor.dir/dk.homestead.sw803f14.Client";
    public static final String CLIENT_ITEM_TYPE = "vnd.android.cursor.item/dk.homestead.sw803f14.Client";
    public static final String FILE_TYPE = "vnd.android.cursor.dir/dk.homestead.sw803f14.File";
    public static final String FILE_ITEM_TYPE = "vnd.android.cursor.item/dk.homestead.sw803f14.File";

    /** Various support/convenience classes for the database schema. **/
    public static final class Files implements BaseColumns {
        private Files() {}

        public static final String NAME = "name";
        public static final String PATH = "path";
        public static final String DOMAIN = "domain_id";
    }

    public static final class Domains implements BaseColumns {
        private Domains() {}

        public static final String NAME = "name";
        public static final String SECRET = "secret";
    }

    public static final class Clients implements BaseColumns {
        private Clients() {}

        public static final String NAME = "name";
    }

    public static final class ClientDomainRelationships implements BaseColumns {
        private ClientDomainRelationships() {}

        public static final String DOMAIN = "domain_id";
        public static final String CLIENT = "client_id";
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            Log.d(TAG + "DatabaseHelper()", "DatabaseHelper being constructed.");
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(TAG + "DatabaseHelper.onCreate()", "Creating tables.");

            for (String s : new String[] {DOMAINS_TABLE, CLIENTS_TABLE, CLIENTS_DOMAINS_TABLE, FILES_TABLE}) {
                db.execSQL("DROP TABLE IF EXISTS " + s);
            }

            // Create domains table.
            db.execSQL("CREATE TABLE " + DOMAINS_TABLE + "("
                + Domains._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Domains.NAME + " TEXT, "
                + Domains.SECRET + " TEXT"
                + ");");

            // Create clients table.
            db.execSQL("CREATE TABLE " + CLIENTS_TABLE + "(" +
                    Clients._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    Clients.NAME + " TEXT);");

            // Create the targetClient-domain relationship table. Many-to-many, messy stuff.
            db.execSQL("CREATE TABLE " + CLIENTS_DOMAINS_TABLE + " (" +
                    ClientDomainRelationships._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ClientDomainRelationships.CLIENT + " INTEGER, " +
                    ClientDomainRelationships.DOMAIN + " INTEGER, " +
                    "FOREIGN KEY (" + ClientDomainRelationships.CLIENT + ") REFERENCES " + CLIENTS_TABLE + " (" + Clients._ID + "), " +
                    "FOREIGN KEY (" + ClientDomainRelationships.DOMAIN + ") REFERENCES " + DOMAINS_TABLE + " (" + Domains._ID + "));");

            // Create the files table.
            db.execSQL("CREATE TABLE " + FILES_TABLE + "(" +
                    Files._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    Files.NAME + " TEXT, " +
                    Files.PATH + " TEXT);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(TAG, "Upgrading SunWukong database from version " + oldVersion + " to " + newVersion + ".");

            onCreate(db);
        }
    }

    /**
     * The database helper that constructs and upgrades the SunWukong database.
     */
    private DatabaseHelper databaseHelper;

    private static UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static HashMap<String, String> projectionMap = new HashMap<>();

    @Override
    public boolean onCreate() {
        Log.d(TAG + "onCreate()", "Creating helper.");
        databaseHelper = new DatabaseHelper(getContext());

        return true;
    }

    /**
     * Query the database for files, domains or clients. Note that this should never be accessible
     * to other applications, only the service package itself.
     * TODO: There are some serious cohesion issues with selection between plural/singular. Fix.
     * @param uri The Uri to query against. Must be a valid content Uri path.
     * @param projection       The columns to project.
     * @param selection
     * @param selectionArgs
     * @param sortOrder The sorting order. Defaults to "name ASC".
     * @return
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(TAG + "query()", "Queried URI '" + uri.toString() + "'.");
        //return databaseHelper.getReadableDatabase().query(FILES_TABLE, new String[] {SunWukongFiles.NAME, SunWukongFiles.PATH}, null, null, null, null, null);

        SQLiteQueryBuilder sb = new SQLiteQueryBuilder();
        // Note to self: projection maps are cute and all if they don't try to select non-existent
        // columns!
        //sb.setProjectionMap(projectionMap);

        int uriMatch = uriMatcher.match(uri);

        switch (uriMatch) {
            case DOMAINS:
                sb.setTables(DOMAINS_TABLE);
                break;
            case DOMAIN_ID:
                sb.setTables(DOMAINS_TABLE);
                sb.appendWhere(Domains._ID + "=" + uri.getPathSegments().get(1));
                break;
            case CLIENTS:
                sb.setTables(CLIENTS_TABLE);
                break;
            case CLIENT_ID:
                sb.setTables(CLIENTS_TABLE);
                sb.appendWhere(Clients._ID + "=" + uri.getPathSegments().get(1));
                break;

            default: throw new IllegalArgumentException("The uri '" + uri + "' could not be matched.");
        }

        sortOrder = sortOrder.isEmpty() ? "name ASC" : sortOrder;

        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        Cursor c = sb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    /**
     * Returns the MIME type matching a specific Uri. As noted in the Android documentation, this
     * method is *always* available to other applications.
     * @param uri The Uri to test against.
     * @return The MIME type constant.
     */
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case DOMAINS: return DOMAIN_TYPE;
            case DOMAIN_ID: return DOMAIN_ITEM_TYPE;
            case CLIENTS: return CLIENT_TYPE;
            case CLIENT_ID: return CLIENT_ITEM_TYPE;
            case FILES: return FILE_TYPE;
            case FILE_ID: return FILE_ITEM_TYPE;
            default: throw new IllegalArgumentException("Unknown Uri '" + uri + "'.");
        }
    }

    /**
     * Inserts new data into the service's database. Principally, external applications will *never*
     * be granted access to this method.
     * @param uri Uri to insert at. Must be one of the plural Uri entry points (domains, clients,
     *            files).
     * @param contentValues key/value set of values to insert. Graceful error detection not
     *                      guaranteed.
     * @return Uri of the inserted element.
     */
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        // Validate Uri.
        /* TODO: Ensure targetClient/domain is never accessible outside of the service package. Do this
            by putting appropriate permissions in the manifest and grant fileId Uri permissions as
            needed.
         */

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        long rowId = 1;
        Uri noteUri;

        switch (uriMatcher.match(uri)) {
            case DOMAINS:
                if (!contentValues.containsKey(Domains.NAME) || !contentValues.containsKey(Domains.SECRET))
                    throw new IllegalArgumentException("Insufficient parameters for new Domain entry.");
                rowId = db.insertOrThrow(DOMAINS_TABLE, null, contentValues);
                noteUri = ContentUris.withAppendedId(DOMAINS_URI, rowId);
                break;
            case CLIENTS:
                if (!contentValues.containsKey(Clients.NAME))
                    throw new IllegalArgumentException("No name given for new Client entry.");
                rowId = db.insertOrThrow(CLIENTS_TABLE, null, contentValues);
                noteUri = ContentUris.withAppendedId(CLIENTS_URI, rowId);
                break;
            case FILES:
                // Check passed values.
                if (!contentValues.containsKey(Files.NAME) || !contentValues.containsKey(Files.PATH))
                    throw new IllegalArgumentException("Insufficient parameters for new File entry.");

                // Ensure fileId integrity.
                String filePath = contentValues.getAsString(Files.PATH);
                File f = new File(filePath);
                if (!f.exists())
                    throw new IllegalArgumentException("The fileId at '" + filePath + "' could not be found.");
                else if (!f.canRead())
                    throw new IllegalArgumentException("The fileId at '" + filePath + "' is not readable.");
                else if (!f.canWrite())
                    throw new IllegalArgumentException("The fileId at '" + filePath + "' is not writeable.");

                rowId = db.insertOrThrow(FILES_TABLE, null, contentValues);
                noteUri = ContentUris.withAppendedId(FILES_URI, rowId);
                break;
            case GRANT_FILE_ACCESS:
                // TODO: This is most likely bugged. Double check passed contentValues as well as calling points in SunWukongService.
                getContext().grantUriPermission(contentValues.getAsString("packageName"), ContentUris.withAppendedId(FILES_URI, contentValues.getAsLong("fileId")), Intent.FLAG_GRANT_READ_URI_PERMISSION);
            default: throw new IllegalArgumentException("Unknown insert Uri '" + uri + "'.");
        }

        // INSERT was successful.
        getContext().getContentResolver().notifyChange(noteUri, null);
        return noteUri;
    }

    /**
     * Deletes the resource at the given Uri or resources matching selection/selectionArgs.
     * @param uri Content Uri to delete from.
     * @param selection WHERE clause. If null and no specific resource was given in uri, an
     *                  exception will be thrown (we don't want full on wipes).
     * @param selectionArgs If the WHERE clause (selection) contained any question marks, this
     *                      parameter must contain their matching values. They will be bound as
     *                      Strings.
     * @return The number of rows deleted.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        // Validate Uri and set basic values.
        String tableName;
        boolean pluralDelete = true; // True: not a single resource, false: single resource.

        switch (uriMatcher.match(uri)) {
            case DOMAIN_ID:
                pluralDelete = false;
            case DOMAINS:
                tableName = DOMAINS_TABLE;
                break;
            case CLIENT_ID:
                pluralDelete = false;
            case CLIENTS:
                tableName = CLIENTS_TABLE;
                break;
            case FILE_ID:
                pluralDelete = false;
            case FILES:
                tableName = FILES_TABLE;
                break;
            default: throw new IllegalArgumentException("Unknown delete Uri '" + uri + "'.");
        }

        if (pluralDelete && (selection == null || selection.isEmpty()))
            throw new IllegalArgumentException("Mass deletes are prohibited.");
        else if (!pluralDelete) {
            selection = Files._ID + "=" + uri.getPathSegments().get(1);
            selectionArgs = null;
        }

        int affected = databaseHelper.getWritableDatabase().delete(tableName, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return affected;
    }

    /**
     * Updates the data layer matching the Uri. Note that only singular Uris are accepted as not a
     * single field in the database allows duplicate entries.
     * @param uri The Uri of the resource to update.
     * @param contentValues New values for the affected resource.
     * @param selection Ignored by this implementation.
     * @param selectionArgs Ignored by this implementation.
     * @return Number of rows affected (always 1).
     */
    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        // Ensure only a singular Uri was passed.
        String id = getIdFromSingularUri(uri);

        String tableName;
        String id_field;

        // TODO: We need a *much* more elegant way of doing this... static HashMap objects?
        // id_field switch - horrible, horrible, horrible.
        switch (uriMatcher.match(uri)) {
            case DOMAIN_ID: id_field = Domains._ID; break;
            case DOMAIN_NAME: id_field = Domains.NAME; break;
            case CLIENT_ID: id_field = Clients._ID; break;
            case CLIENT_NAME: id_field = Clients.NAME; break;
            case FILE_ID: id_field = Files._ID; break;
            case FILE_NAME: id_field = Files.NAME; break;
            default: throw new IllegalArgumentException("Unknown/unsupported Uri '" + uri + "'.");
        }

        // tableName switch - GOD this is horrible.
        switch (uriMatcher.match(uri)) {
            case DOMAIN_ID:
            case DOMAIN_NAME:
                tableName = DOMAINS_TABLE;
                break;
            case CLIENT_ID:
            case CLIENT_NAME:
                tableName = CLIENTS_TABLE;
                break;
            case FILE_ID:
            case FILE_NAME:
                tableName = FILES_TABLE;
                break;
            default: throw new IllegalArgumentException("Unknown/unsupported Uri '" + uri + "'.");
        }

        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        return db.update(tableName, contentValues, id_field + "=" + id, null);
    }

    /**
     * Opens a fileId known to the service for reading. This method is intended to be used for copying
     * fileId data from the service's data store to that of your own application.
     * TODO: should be support write modes as well? Allow some sort of shared store... one hell of
     * a corruption issue.
     * @param uri Uri path to the fileId. This must be the fileId's id (i.e. "files/#").
     * @param mode File mode. Currently, only "r" is supported.
     * @return Object to read the fileId.
     * @throws FileNotFoundException
     */
    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        // Only allow these requests with FILE_ID uris.
        int match = uriMatcher.match(uri);
        String id = uri.getPathSegments().get(1);
        if (match != FILE_ID && match != FILE_NAME) throw new IllegalArgumentException("The Uri '" + uri + "' is not supported for fileId operations.");

        if (mode.compareTo("r") != 0) throw new IllegalArgumentException("Non-read modes are not supported.");

        // Find matching fileId, by id.
        String fileId = uri.getPathSegments().get(1);

        SQLiteQueryBuilder sb = new SQLiteQueryBuilder();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        sb.setTables(FILES_TABLE);
        // Run the query! ... much too simple query?
        Cursor c = sb.query(db, null, (match == FILE_ID ? Files._ID : Files.NAME) + "=" + id, null, null, null, null); // ... too simple?

        // Ensure we got a hit.
        if (c.getCount() == 0)
            throw new IllegalArgumentException("The fileId with id " + fileId + " does not exist.");

        // Superfluous?
        c.moveToFirst();

        // Get the path, test the fileId, return it.
        String filePath = c.getString(c.getColumnIndex(Files.PATH));
        File f = new File(filePath);
        if (!f.exists())
            throw new FileNotFoundException("File with id '" + fileId + "' could not be found.");
        else if (!f.canRead())
            throw new IllegalArgumentException("The fileId with id '" + fileId + "' cannot be read. THIS SHOULD NEVER HAPPEN!");

        return ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_ONLY);
    }

    /**
     * Shuts doesn the provider. Strictly not necessary, but useful for unit testing purposes.
     */
    @Override
    public void shutdown() {
        super.shutdown();
    }

    /**
     * Tests a Uri to see if it matches against any Uri that uniquely identifies a single resource,
     * RESET style.
     * @param uri The Uri to test.
     * @return True if the Uri points to a single resource, false otherwise.
     */
    public static boolean isSingularUri(Uri uri) {
        ArrayList<Integer> singulars = new ArrayList<>();
        singulars.add(CLIENT_ID);
        singulars.add(CLIENT_NAME);
        singulars.add(DOMAIN_ID);
        singulars.add(FILE_NAME);
        singulars.add(FILE_ID);
        singulars.add(DOMAIN_NAME);

        return singulars.contains(uriMatcher.match(uri));
    }

    /**
     * Retrieves the id part of a Uri that points to a single resource. For example, for domains/3
     * it would return "3". For files/chatterbox.txt it would return "chatterbox.txt".
     * @param uri The Uri to test against.
     * @return The identifying part of the Uri, see above.
     * @throws IllegalArgumentException Thrown if the Uri does not point to a single resource.
     */
    public static String getIdFromSingularUri(Uri uri) throws IllegalArgumentException {
        if (isSingularUri(uri)) return uri.getPathSegments().get(1);
        else throw new IllegalArgumentException("The Uri '" + uri + "' does not point to a single resource.");
    }

    // TODO: Clean up numbering, for posterity's sake.
    private static final int CLIENT_ID = 1;
    private static final int CLIENT_NAME = 7;
    private static final int CLIENTS = 2;
    private static final int DOMAIN_ID = 3;
    private static final int DOMAINS = 4;
    private static final int DOMAIN_NAME = 9;
    private static final int FILE_ID = 5;
    private static final int FILES = 6;
    private static final int FILE_NAME = 8;
    private static final int GRANT_FILE_ACCESS = 10;

    static {
        uriMatcher.addURI(AUTHORITY, "clients", CLIENTS);
        uriMatcher.addURI(AUTHORITY, "clients/#", CLIENT_ID);
        uriMatcher.addURI(AUTHORITY, "clients/*", CLIENT_NAME);
        uriMatcher.addURI(AUTHORITY, "domains", DOMAINS);
        uriMatcher.addURI(AUTHORITY, "domains/#", DOMAIN_ID);
        uriMatcher.addURI(AUTHORITY, "files", FILES);
        uriMatcher.addURI(AUTHORITY, "files/#", FILE_ID);
        uriMatcher.addURI(AUTHORITY, "files/*", FILE_NAME);
        uriMatcher.addURI(AUTHORITY, "files/#/grant/*", GRANT_FILE_ACCESS);
        uriMatcher.addURI(AUTHORITY, "files/*/grant/*", GRANT_FILE_ACCESS);

        projectionMap.put(Domains._ID, Domains._ID);
        projectionMap.put(Domains.NAME, Domains.NAME);
        projectionMap.put(Domains.SECRET, Domains.SECRET);
        projectionMap.put(Clients._ID, Clients._ID);
        projectionMap.put(Clients.NAME, Clients.NAME);
        projectionMap.put(Files._ID, Files._ID);
        projectionMap.put(Files.NAME, Files.NAME);
        projectionMap.put(Files.PATH, Files.PATH);
        projectionMap.put(ClientDomainRelationships._ID, ClientDomainRelationships._ID);
        projectionMap.put(ClientDomainRelationships.CLIENT, ClientDomainRelationships.CLIENT);
        projectionMap.put(ClientDomainRelationships.DOMAIN, ClientDomainRelationships.DOMAIN);
    }
}
