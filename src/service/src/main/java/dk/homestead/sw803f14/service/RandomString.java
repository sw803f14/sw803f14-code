package dk.homestead.sw803f14.service;

import java.util.Random;

/**
 * Generates pseudo-random (not very strong) Strings.
 * Created by lindhart on 5/14/14.
 */
public class RandomString {
    /**
     * Generates a pseudo-random String to be kept inside the Domain and isolated within DomainKey
     * objects.
     * @return The *new* key. It is impossible to retrieve the existing key using this method.
     */
    public static String generate(int keyLength) {
        char[] good_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/*-+!#¤%&()".toCharArray();
        Random random = new Random();

        String retVal = "";

        for (int i = 0; i < keyLength; i++) {
            retVal += good_chars[random.nextInt(good_chars.length)];
        }

        return retVal;
    }

    public static String generate() {
        return generate(32);
    }
}
