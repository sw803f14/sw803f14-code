package dk.homestead.sw803f14.service;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the binding to a targetClient.
 * Created by lindhart on 5/7/14.
 */
public class SunWukongClient {
    private final String privateKey;
    /**
     * The targetClient-chosen name for itself. As long as it's unique, we don't care if it's not the
     * package.
     */
    private String clientName;

    /**
     * The targetClient's package name. Essential to be correct if the targetClient needs later fileId access.
     */
    private String clientPackage;
    private ISunWukongClient clientInterface;
    private List<SunWukongOpenDomain> _openDomains = new ArrayList <>();
    private List<SunWukongClosedDomainBase> _closedDomains = new ArrayList <>();

    public SunWukongClient(String clientName, String packageName, ISunWukongClient clientInterface) {
        this.clientName = clientName;
        this.clientInterface = clientInterface;
        this.clientPackage = packageName;
        this.privateKey = RandomString.generate(32);
    }

    public final String clientName() { return clientName; }
    public final String packageName() { return clientPackage; }

    public final ClientKey getClientKey() {
        return new ClientKey(clientName(), packageName(), privateKey);
    }

    public final ISunWukongClient getClientInterface() {
        return clientInterface;
    }

    @Override
    public String toString() {
        return clientName();
    }

    protected void onOpenDomainJoined(SunWukongOpenDomain domain) {
        _openDomains.add(domain);
    }

    protected void onClosedDomainJoined(SunWukongClosedDomainBase domain) {
        _closedDomains.add(domain);
    }

    public void unbind() {
        for (SunWukongOpenDomain d : _openDomains) {
            d.removeClient(getClientKey());
        }
        for (SunWukongClosedDomainBase d : _closedDomains) {
            d.removeClient(getClientKey());
        }
    }

    /**
     * Created by lindhart on 5/9/14.
     * A ClientKey is what a targetClient is given to uniquely identify themselves to the service in the
     * future.
     * Essentially the ClientKey is a weak copy of the real targetClient reference. Since we need to
     * marshal this between Service and Client, a reference is both dangerous and costly. Instead,
     * the ClientKey contains just enough to find the correct targetClient entry in the service, vet it,
     * and work with it. The reasoning is the clients are allowed to have ClientKey instances, but
     * they cannot instantiate or fake them (hopefully).
     * TODO: Figure out out to properly encapsulate this concept so the key cannot be (easily) faked.
     */
    public static class ClientKey implements Parcelable {
        static Creator<ClientKey> CREATOR = new Creator<ClientKey>() {
            @Override
            public ClientKey createFromParcel(Parcel parcel) {
                return new ClientKey(parcel.readString(), parcel.readString(), parcel.readString());
            }

            @Override
            public ClientKey[] newArray(int i) {
                return new ClientKey[0];
            }
        };

        private final String clientName;
        private final String packageName;
        private final String privateKey;

        private ClientKey(String clientName, String packageName, String privateKey) {
            this.clientName = clientName;
            this.privateKey = privateKey;
            this.packageName = packageName;
        }

        public String clientName() {
            return this.clientName;
        }
        public String packageName() { return this.packageName; }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(clientName());
            parcel.writeString(packageName());
            parcel.writeString(privateKey);
        }
    }
}
