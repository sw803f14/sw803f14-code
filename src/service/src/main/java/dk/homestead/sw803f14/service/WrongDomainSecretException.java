package dk.homestead.sw803f14.service;

/**
 * Thrown when the wrong secret is used when attempting to join a closed domain.
 * Created by lindhart on 5/9/14.
 */
public class WrongDomainSecretException extends Exception {
    public WrongDomainSecretException(String msg) {
        super(msg);
    }
}
