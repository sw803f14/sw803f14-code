package dk.homestead.sw803f14.service.file.operands;

/**
 * The simplest operand, testing equality.
 * Created by lindhart on 5/9/14.
 */
public class EqualsOperand extends ComparisonOperand {
    public EqualsOperand(String key, String value) {
        super(key, "==", value);
    }

    @Override
    public boolean match(String test) {
        return value.equals(test);
    }
}
