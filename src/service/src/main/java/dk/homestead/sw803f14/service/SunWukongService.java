package dk.homestead.sw803f14.service;

import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dk.homestead.sw803f14.service.provider.SunWukongProvider;

public class SunWukongService extends Service {
    public static final int REGISTER_SUCCESS = 1;
    public static final int REGISTER_EXISTS = 2;
    public static final int UNKNOWN_DOMAIN = 3;
    public static final int CLIENT_ALREADY_IN_DOMAIN = 4;
    public static final int CLIENT_DENIED_ACCESS_TO_DOMAIN = 5;
    public static final int UNKNOWN_CLIENT = 6;
    public static final int DOMAIN_JOIN_SUCCESS = 7;
    public static final int DOMAIN_LEAVE_SUCCESSFUL = 8;

    protected static String tag(String sub) { return "SunWukongService." + sub; }

    /**
     * Collection of all registered clients.
     */
    protected final HashMap<String, SunWukongClient> registeredClients = new HashMap<>();
    private final ISunWukongService swService = new ISunWukongService.Stub() {
        private String tag(String sub) { return SunWukongService.tag("ISunWukongService." + sub); }

        @Override
        public int registerClient(String clientName, String packageName, ISunWukongClient clientInterface) throws RemoteException {
            if (registeredClients.containsKey(clientName)) return REGISTER_EXISTS;
            else {
                SunWukongClient swc = new SunWukongClient(clientName, packageName, clientInterface);
                // Lock registeredClients to this operation, avoiding multiple writes to the same entry.
                synchronized (registeredClients) {
                    registeredClients.put(clientName, swc);
                }
                clientInterface.onClientRegistered(swc.getClientKey());
                return REGISTER_SUCCESS;
            }
        }

        @Override
        public void unregisterClient(SunWukongClient.ClientKey key) {
            try {
                SunWukongClient swc = getClient(key);
                swc.unbind(); // Remove from domains.
            } catch (ClientNotFoundException e) {
                e.printStackTrace();
                // return UNKNOWN_CLIENT;
            }
        }

        @Override
        public int joinOpenClientDomain(SunWukongClient.ClientKey key, String domainName) throws RemoteException {
            if (openDomains.containsKey(domainName)) {
                openDomains.get(domainName).addClient(key);
                return DOMAIN_JOIN_SUCCESS;
            } else return UNKNOWN_DOMAIN;
        }

        @Override
        public int joinClosedClientDomain(SunWukongClient.ClientKey key, String domainName, String secret) throws RemoteException {
            if (closedDomains.containsKey(domainName)) {
                try {
                    closedDomains.get(domainName).addClient(key, secret);
                    return DOMAIN_JOIN_SUCCESS;
                } catch (WrongDomainSecretException e) {
                    e.printStackTrace();
                    return CLIENT_DENIED_ACCESS_TO_DOMAIN;
                }
            } else return UNKNOWN_DOMAIN;
        }

        @Override
        public int leaveOpenClientDomain(SunWukongClient.ClientKey key, String domainName) throws RemoteException {
            if (openDomains.containsKey(domainName)) {
                openDomains.get(domainName).removeClient(key);
                return DOMAIN_LEAVE_SUCCESSFUL;
            }
            else return UNKNOWN_DOMAIN;
        }

        @Override
        public int leaveClosedClientDomain(SunWukongClient.ClientKey key, String domainName) throws RemoteException {
            if (closedDomains.containsKey(domainName)) {
                closedDomains.get(domainName).removeClient(key);
                return DOMAIN_LEAVE_SUCCESSFUL;
            }
            else return UNKNOWN_DOMAIN;
        }

        @Override
        public FileInfo getFileInfo(SunWukongClient.ClientKey key, SunWukongDomain.DomainKey domain, String fileId) throws RemoteException {
            // FileInfo to return.
            FileInfo foundFile = null;

            // Retrieve the relevant domain.
            SunWukongDomain swDomain;
            try {
                swDomain = getDomain(domain);
            } catch (DomainNotFoundException e) {
                e.printStackTrace();
                return null;
            }

            // Request the fileId sequentially from every targetClient until one has found it (or none).
            for (SunWukongClient.ClientKey swClient : swDomain.getClients()) {
                try {
                    foundFile = getClient(swClient).getClientInterface().onGetFileInfo(domain, fileId);
                } catch (ClientNotFoundException e) {
                    // Cleanup!
                    swDomain.removeClient(swClient);
                    e.printStackTrace();
                }
                if (foundFile != null) break;
            }

            // If the fileId was not found, seek on the network.
            // TODO: Implement.
            Log.e(tag("getFileInfo"), "TODO: Network request.");

            // If the fileId was nowhere to be found, return null.
            return null;
        }

        @Override
        public FileInfo findFileInfo(SunWukongClient.ClientKey key, SunWukongDomain.DomainKey domain, StringTriplet[] criteria) throws RemoteException {
            // FileInfo to return.
            FileInfo foundFile = null;

            // Retrieve the relevant domain.
            SunWukongDomain swDomain;
            try {
                swDomain = getDomain(domain);
            } catch (DomainNotFoundException e) {
                e.printStackTrace();
                return null;
            }

            // Request the fileId sequentially from every targetClient until one has found it (or none).
            for (SunWukongClient.ClientKey swClient : swDomain.getClients()) {
                try {
                    foundFile = getClient(swClient).getClientInterface().onFindFileInfo(domain, criteria);
                    if (foundFile != null) {

                        break;
                    }
                } catch (ClientNotFoundException e) {
                    // Client no longer exists. Cleanup!
                    swDomain.removeClient(swClient);
                    e.printStackTrace();
                }
            }

            // If the fileId was not found, seek on the network.
            // TODO: Implement.
            Log.e(tag("findFileInfo"), "TODO: Network request.");

            // If the fileId was nowhere to be found, return null.
            return null;
        }

        @Override
        public void retrieveFile(SunWukongClient.ClientKey client, SunWukongDomain.DomainKey domain, FileInfo fileId, ISunWukongFileDownloadListener listener) throws RemoteException {
            FileTransfer transfer = new FileTransfer(fileId, client, domain, listener);
            activeTransfers.add(transfer);
            transfer.run();
        }

        @Override
        public void streamFile(SunWukongClient.ClientKey client, SunWukongDomain.DomainKey domain, String fileId, dk.homestead.sw803f14.service.IFileTransferHandler handler) throws RemoteException {
            handler.onBegin();

            // Retrieve fileId handler.
            try {
                ParcelFileDescriptor pfd = getContentResolver().openFileDescriptor(ContentUris.withAppendedId(SunWukongProvider.FILES_URI, Long.getLong(fileId)), "r");
                FileInputStream stream = new FileInputStream(pfd.getFileDescriptor());

                int maxBytes = 8; // TODO: Fix constant max bytes.
                byte[] bytes = new byte[maxBytes];

                while(stream.read(bytes) != -1) {
                    handler.onByte(bytes);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            handler.onComplete();
        }

        @Override
        public ParcelFileDescriptor openFile(SunWukongClient.ClientKey client, SunWukongDomain.DomainKey domain, FileInfo fileId) throws RemoteException {
            try {
                return ParcelFileDescriptor.open(new File(getFilesDir(), domain.domainName() + "_" + fileId.filename()), ParcelFileDescriptor.MODE_READ_ONLY);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }
    };

    protected void grantFileAccess(SunWukongClient.ClientKey client, String fileId) {
        // TODO: Consider this interface. Should the packagename (and possibly fileId) be part of the content values instead?
        getContentResolver().insert(SunWukongProvider.makeFileAccessUri(fileId, client.packageName()), null);
    }

    /**
     * Collection of all closed (password-protected) domains.
     */
    protected HashMap<String, SunWukongClosedDomainBase> closedDomains = new HashMap<>();
    /**
     * List of open domains, freely accessible by all apps.
     */
    protected HashMap<String, SunWukongOpenDomain> openDomains = new HashMap<>();
    /**
     * List of all active transfers for all clients.
     */
    protected List<FileTransfer> activeTransfers = new ArrayList<>();

    public SunWukongService() {
    }

    protected SunWukongClient getClient(SunWukongClient.ClientKey key) throws ClientNotFoundException {
        SunWukongClient swc = registeredClients.get(key.clientName());
        if (swc == null) throw new ClientNotFoundException("The targetClient " + key.clientName() + " is not registered.");
        else return swc;
    }

    protected SunWukongDomain getDomain(SunWukongDomain.DomainKey key) throws DomainNotFoundException {
        SunWukongDomain swd = key.isOpen() ? openDomains.get(key.domainName()) : closedDomains.get(key.domainName());
        if (swd == null) throw new DomainNotFoundException("The domain " + key.domainName() + " doesn't exist.");
        else return swd;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return our interface instance as a binder, ready for use at the calling end.
        return swService.asBinder();
    }

    /**
     * FileTransfer represents the transfer of a specific fileId from a targetClient in a specific domain. It
     * essentially wraps the key data required to facilitate a deferred transfer.
     * Created by lindhart on 5/13/14.
     */
    public class FileTransfer implements Runnable {
        protected FileInfo file;
        protected SunWukongClient targetClient;
        protected SunWukongClient sourceClient;
        protected SunWukongDomain domain;
        protected ISunWukongFileDownloadListener listener;

        public final static String STATUS_COMPLETED = "COMPLETED";
        public final static String STATUS_DOWNLOADING = "DOWNLOADING";
        public final static String STATUS_QUERYING = "QUERYING";
        public final static String STATUS_INITIALIZED = "INITIALIZED";
        public final static String STATUS_FAILED = "FAILED";

        public FileTransfer(FileInfo targetFile, SunWukongClient.ClientKey targetClient, SunWukongDomain.DomainKey domain, ISunWukongFileDownloadListener listener) throws RemoteException {
            // Set a healthy priority.
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            this.listener = listener;
            file = targetFile;
            listener.onStatus(STATUS_INITIALIZED);
            try {
                this.targetClient = getClient(targetClient);
                this.sourceClient = getClient(file.getSourceClient());
                this.domain = getDomain(domain);
            } catch (ClientNotFoundException e) {
                listener.onStatus(STATUS_FAILED);
                listener.onFailed("Unknown Client attempting download or source client no longer exists.");
                e.printStackTrace();
            } catch (DomainNotFoundException e) {
                listener.onStatus(STATUS_FAILED);
                listener.onFailed("Invalid Domain to download from.");
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            try {
                listener.onStatus(STATUS_QUERYING);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            try {
                ParcelFileDescriptor pfd = sourceClient.getClientInterface().onOpenFile(domain.getDomainKey(), file.filename());
                FileInputStream in = new FileInputStream(pfd.getFileDescriptor());
                if (pfd == null) {
                    listener.onStatus(STATUS_FAILED);
                    listener.onFailed("The file is no longer available from provider. Perform a new search.");
                } else {
                    // Got a handler. Open file and start transferring.
                    listener.onStatus(STATUS_DOWNLOADING);
                    FileOutputStream out = openFileOutput(domain.domainName() + "_" + file.filename(), MODE_PRIVATE);

                    // Local handlers, large chunks not an issue.
                    byte buf[] = new byte[1024];
                    int len;
                    int progress = 0;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                        progress += len;
                        listener.onProgress(progress, pfd.getStatSize());
                    }
                    in.close();
                    out.close();
                    listener.onStatus(STATUS_COMPLETED);
                    listener.onCompleted();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (IOException e) {
                // We should never get this. Opening a new file for write should not fail.
                // MAYBE we get this for unforeseen write/read issues (capacity?).
                e.printStackTrace();
            }
        }
    }

}
