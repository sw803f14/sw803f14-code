package dk.homestead.sw803f14.service.file.operands;

/**
 * The ComparisonOperand represents an operation on some value. ComparisonOperands lie in the space
 * between lambda expressions (which are unavailable in Java7) and strict typing used to ensure
 * specific operand types.
 * Created by lindhart on 5/9/14.
 */
public abstract class ComparisonOperand {
    protected String key;
    protected String value;
    protected String operation;

    public ComparisonOperand(String key, String op, String val) {
        this.key = key;
        value = val;
        operation = op;
    }

    /**,
     * Runs the operand and tries to perform its match.
     * @param test The value to test with.
     * @return True if the test was successful, false otherwise.
     */
    public abstract boolean match(String test);
}
