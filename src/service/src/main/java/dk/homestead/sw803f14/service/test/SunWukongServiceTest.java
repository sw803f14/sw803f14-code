package dk.homestead.sw803f14.service.test;

import android.content.Intent;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.test.ServiceTestCase;
import android.util.Log;

import dk.homestead.sw803f14.service.FileInfo;
import dk.homestead.sw803f14.service.ISunWukongClient;
import dk.homestead.sw803f14.service.ISunWukongService;
import dk.homestead.sw803f14.service.StringTriplet;
import dk.homestead.sw803f14.service.SunWukongClient.ClientKey;
import dk.homestead.sw803f14.service.SunWukongDomain.DomainKey;
import dk.homestead.sw803f14.service.SunWukongService;

/**
 * Test case for the SunWukonService class.
 * TODO: Consider moving this into several cases, each testing an aspect (domains, registration,
 * fileId info, etc).
 * Created by lindhart on 5/14/14.
 */
public class SunWukongServiceTest extends ServiceTestCase {
    private ISunWukongService serviceBinder;

    /**
     * A completely inactive targetClient binder. Generally used when we are not interested in callbacks,
     * but return values of serviceBinder.
     */
    private ISunWukongClient passiveClientBinder = new ISunWukongClient.Stub() {

        @Override
        public void onClientRegistered(ClientKey clientKey) throws RemoteException {

        }

        @Override
        public void onClientUnregistered() throws RemoteException {

        }

        @Override
        public void onDomainJoined(DomainKey key) throws RemoteException {

        }

        @Override
        public void onDomainLeft(DomainKey key) throws RemoteException {

        }

        @Override
        public ParcelFileDescriptor onOpenFile(DomainKey domain, String fileId) throws RemoteException {
            return null;
        }

        @Override
        public FileInfo onGetFileInfo(DomainKey domain, String fileId) throws RemoteException {
            return null;
        }

        @Override
        public FileInfo onFindFileInfo(DomainKey domain, StringTriplet[] criteria) throws RemoteException {
            return null;
        }
    };

    private String clientName = "TestClient-1";
    private String packageName = "dk.homestead.sw803f14.test.SunWukongServiceTest";

    public SunWukongServiceTest() {
        super(SunWukongService.class);
    }

    @Override
    protected void setUp() throws Exception {
        serviceBinder = ISunWukongService.Stub.asInterface(bindService(new Intent(getContext(), SunWukongService.class)));
        super.setUp();
    }

    public void testSingleRegisterClient() throws Exception {
        serviceBinder.registerClient(clientName, packageName, new ISunWukongClient.Stub() {

            @Override
            public void onClientRegistered(ClientKey clientKey) throws RemoteException {
                assertEquals(clientName, clientKey.clientName());
                Log.d("SunWukongServiceTest:testRegisterClient", "Assuming successful assert.");
            }

            @Override
            public void onClientUnregistered() throws RemoteException {

            }

            @Override
            public void onDomainJoined(DomainKey key) throws RemoteException {

            }

            @Override
            public void onDomainLeft(DomainKey key) throws RemoteException {

            }

            @Override
            public ParcelFileDescriptor onOpenFile(DomainKey domain, String fileId) throws RemoteException {
                return null;
            }

            @Override
            public FileInfo onGetFileInfo(DomainKey domain, String fileId) throws RemoteException {
                return null;
            }

            @Override
            public FileInfo onFindFileInfo(DomainKey domain, StringTriplet[] criteria) throws RemoteException {
                return null;
            }
        });
    }

    public void testDuplicateRegisterClient()  throws Exception {
        assertEquals(SunWukongService.REGISTER_SUCCESS, serviceBinder.registerClient(clientName, packageName, passiveClientBinder));
        assertEquals(SunWukongService.REGISTER_EXISTS, serviceBinder.registerClient(clientName, packageName, passiveClientBinder));
    }

    public void testSingleOpenDomainJoin() throws Exception {
        ISunWukongClient clientBinder = new ISunWukongClient.Stub() {
            public ClientKey myKey;

            @Override
            public void onClientRegistered(ClientKey clientKey) throws RemoteException {
                assertEquals(clientName, clientKey.clientName());
                myKey = clientKey;
            }

            @Override
            public void onClientUnregistered() throws RemoteException {
                myKey = null;
            }

            @Override
            public void onDomainJoined(DomainKey key) throws RemoteException {

            }

            @Override
            public void onDomainLeft(DomainKey key) throws RemoteException {

            }

            @Override
            public ParcelFileDescriptor onOpenFile(DomainKey domain, String fileId) throws RemoteException {
                return null;
            }

            @Override
            public FileInfo onGetFileInfo(DomainKey domain, String fileId) throws RemoteException {
                return null;
            }

            @Override
            public FileInfo onFindFileInfo(DomainKey domain, StringTriplet[] criteria) throws RemoteException {
                return null;
            }
        };
        // Ensure registration.
        assertEquals(SunWukongService.REGISTER_SUCCESS, serviceBinder.registerClient(clientName, packageName, clientBinder));
        // Attempt to join open domain.

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
