package dk.homestead.sw803f14.service;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lindhart on 5/19/14.
 */
public class StringTriplet extends Triplet<String,String,String> implements Parcelable {
    static Creator<StringTriplet> CREATOR = new Creator<StringTriplet>() {
        @Override
        public StringTriplet createFromParcel(Parcel parcel) {
            return new StringTriplet(parcel.readString(), parcel.readString(), parcel.readString());
        }

        @Override
        public StringTriplet[] newArray(int i) {
            return new StringTriplet[0];
        }
    };

    public StringTriplet() {
        left = center = right = "";
    }

    public StringTriplet(String left, String center, String right) {
        super(left, center, right);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(left);
        parcel.writeString(center);
        parcel.writeString(right);
    }
}
