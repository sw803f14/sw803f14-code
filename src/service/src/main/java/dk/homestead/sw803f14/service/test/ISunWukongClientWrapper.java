package dk.homestead.sw803f14.service.test;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

import dk.homestead.sw803f14.service.FileInfo;
import dk.homestead.sw803f14.service.ISunWukongClient;
import dk.homestead.sw803f14.service.StringTriplet;
import dk.homestead.sw803f14.service.SunWukongClient;
import dk.homestead.sw803f14.service.SunWukongDomain;

/**
 * Created by lindhart on 5/14/14.
 */
public class ISunWukongClientWrapper {
    public static ISunWukongClient getInstance() {
        return new ISunWukongClient.Stub() {

            @Override
            public void onClientRegistered(SunWukongClient.ClientKey clientKey) throws RemoteException {

            }

            @Override
            public void onClientUnregistered() throws RemoteException {

            }

            @Override
            public void onDomainJoined(SunWukongDomain.DomainKey key) throws RemoteException {

            }

            @Override
            public void onDomainLeft(SunWukongDomain.DomainKey key) throws RemoteException {

            }

            @Override
            public ParcelFileDescriptor onOpenFile(SunWukongDomain.DomainKey domain, String fileId) throws RemoteException {
                return null;
            }

            @Override
            public FileInfo onGetFileInfo(SunWukongDomain.DomainKey domain, String fileId) throws RemoteException {
                return null;
            }

            @Override
            public FileInfo onFindFileInfo(SunWukongDomain.DomainKey domain, StringTriplet[] criteria) throws RemoteException {
                return null;
            }
        };
    }
}
